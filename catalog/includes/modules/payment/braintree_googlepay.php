<?php
// Include Braintree SDK - Adjust the path based on your project structure
require_once(DIR_FS_CATALOG . DIR_WS_MODULES . 'payment/braintree/lib/Braintree.php');  // Same as in braintree_api.php

class braintree_googlepay extends base {
    /**
     * $code determines the internal 'code' name used to designate "this" payment module
     * @var string
     */
    var $code = 'braintree_googlepay'; // Google Pay

    /**
     * Module version
     * @var string
     */
    var $version = '2025-02-13';

    /**
     * $title is the displayed name for this payment method
     * @var string
     */
    var $title = 'Google Pay';

    /**
     * $description is a soft name for this payment method
     * @var string
     */
    var $description = 'Google Pay Payment Option';

    /**
     * $enabled determines whether this module shows or not... in catalog.
     * @var boolean
     */
    var $enabled = false;

    /**
     * API keys
     */
    private $gateway_mode;
    private $client_token;

    /**
     * Constructor
     */
    function __construct() {
        global $order;

        $this->title = MODULE_PAYMENT_BRAINTREE_GOOGLE_PAY_TEXT_ADMIN_TITLE;
        $this->description = MODULE_PAYMENT_BRAINTREE_GOOGLE_PAY_TEXT_ADMIN_DESCRIPTION;

        $this->sort_order = defined('MODULE_PAYMENT_BRAINTREE_GOOGLE_PAY_SORT_ORDER') ? MODULE_PAYMENT_BRAINTREE_GOOGLE_PAY_SORT_ORDER : null;
        $this->enabled = defined('MODULE_PAYMENT_BRAINTREE_GOOGLE_PAY_STATUS') && MODULE_PAYMENT_BRAINTREE_GOOGLE_PAY_STATUS == 'True';

        $this->zone = (int) (defined('MODULE_PAYMENT_BRAINTREE_GOOGLE_PAY_ZONE')) ? MODULE_PAYMENT_BRAINTREE_GOOGLE_PAY_ZONE : 0;
    }

    /**
     *  Sets payment module status based on zone restrictions etc
     */
    function update_status() {
        global $order, $db;

        // check other reasons for the module to be deactivated:
        if ($this->enabled && (int) $this->zone > 0) {

            $check_flag = false;

            $sql = "SELECT zone_id
                FROM " . TABLE_ZONES_TO_GEO_ZONES . "
                WHERE geo_zone_id = :zoneId
                AND zone_country_id = :countryId
                ORDER BY zone_id";

            $sql = $db->bindVars($sql, ':zoneId', $this->zone, 'integer');
            $sql = $db->bindVars($sql, ':countryId', $order->billing['country']['id'], 'integer');
            $check = $db->Execute($sql);

            while (!$check->EOF) {

                if ($check->fields['zone_id'] < 1) {
                    $check_flag = true;
                    break;
                } else if ($check->fields['zone_id'] == $order->billing['zone_id']) {
                    $check_flag = true;
                    break;
                }

                $check->MoveNext();
            }

            if (!$check_flag) {
                $this->enabled = false;
            }
        }
    }

    /**
     * Get the Braintree Gateway instance.
     *
     * @return Braintree_Gateway The Braintree gateway instance
     */
    function get_braintree_gateway() {
        // Ensure the Braintree credentials are defined and valid
        if (
            !defined("MODULE_PAYMENT_BRAINTREE_GOOGLE_PAY_SERVER") ||
            !defined("MODULE_PAYMENT_BRAINTREE_GOOGLE_PAY_MERCHANT_KEY") ||
            !defined("MODULE_PAYMENT_BRAINTREE_GOOGLE_PAY_PUBLIC_KEY") ||
            !defined("MODULE_PAYMENT_BRAINTREE_GOOGLE_PAY_PRIVATE_KEY")
        ) {
            // Handle the error (for example, with a session message)
            global $messageStack;
            $messageStack->add_session("Braintree configuration not set!", 'error');
            return null;
        }

        // Return the Braintree gateway instance with credentials for Braintree
        $gateway = new Braintree\Gateway([
            'environment' => MODULE_PAYMENT_BRAINTREE_GOOGLE_PAY_SERVER, // Use the Braintree environment defined in the configuration
            'merchantId' => MODULE_PAYMENT_BRAINTREE_GOOGLE_PAY_MERCHANT_KEY,
            'publicKey' => MODULE_PAYMENT_BRAINTREE_GOOGLE_PAY_PUBLIC_KEY,
            'privateKey' => MODULE_PAYMENT_BRAINTREE_GOOGLE_PAY_PRIVATE_KEY
        ]);

        return $gateway;
    }

    /**
     * Generate the Braintree client token for Google Pay
     */
    function generate_client_token() {
        $gateway = $this->get_braintree_gateway(); // Use $this->get_braintree_gateway()

        if ($gateway === null) {
            return ''; // Return an empty client token if the gateway was not initialized properly
        }

        // Generate client token for the frontend (NO tokenizationKey needed here)
        return $gateway->clientToken()->generate();
    }

    /**
     * Display Google Pay Button on the Checkout Payment Page
     */
    function selection() {
        global $order;

        $googleMerchantId = MODULE_PAYMENT_BRAINTREE_GOOGLE_PAY_MERCHANT_ID;
        $googlePayEnvironment = MODULE_PAYMENT_BRAINTREE_GOOGLE_PAY_ENVIRONMENT;
        $braintreeMerchantId = MODULE_PAYMENT_BRAINTREE_GOOGLE_PAY_MERCHANT_KEY;
        $braintreeTokenizationKey = MODULE_PAYMENT_BRAINTREE_GOOGLE_PAY_TOKENIZATION_KEY;

        $clientToken = $this->generate_client_token();

        $output = '<script src="https://js.braintreegateway.com/web/3.115.2/js/client.min.js"></script>';
        $output .= '<script src="https://js.braintreegateway.com/web/3.115.2/js/google-payment.min.js"></script>';
        $output .= '<script src="https://pay.google.com/gp/p/js/pay.js"></script>';

        $output .= '<script>
            "use strict";

            // Ensure googlePayInstance is globally accessible without redeclaring
            if (typeof window.googlePayInstance === "undefined") {
                window.googlePayInstance = null;
            }

            function initializeGooglePay() {
                var clientToken = "' . $clientToken . '";
                if (!clientToken) {
                    console.error("Braintree client token is empty.");
                    return;
                }

                braintree.client.create({
                    authorization: clientToken
                }).then(function(clientInstance) {
                    return braintree.googlePayment.create({
                        client: clientInstance,
                        googlePayVersion: 2,
                        googleMerchantId: "' . $googleMerchantId . '"
                    });
                }).then(function(instance) {
                    googlePayInstance = instance;

                    var paymentsClient = new google.payments.api.PaymentsClient({
                        environment: "' . $googlePayEnvironment . '"
                    });

                    var paymentDataRequest = {
                        apiVersion: 2,
                        apiVersionMinor: 0,
                        allowedPaymentMethods: [{
                            type: "CARD",
                            parameters: {
                                allowedAuthMethods: ["PAN_ONLY", "CRYPTOGRAM_3DS"],
                                allowedCardNetworks: ["VISA", "MASTERCARD"]
                            },
                            tokenizationSpecification: {
                                type: "PAYMENT_GATEWAY",
                                parameters: {
                                    "gateway": "braintree",
                                    "braintree:apiVersion": "v1",
                                    "braintree:sdkVersion": braintree.client.VERSION,
                                    "braintree:merchantId": "' . $braintreeMerchantId . '",
                                    "braintree:clientKey": "' . $braintreeTokenizationKey . '"
                                }
                            }
                        }],
                        transactionInfo: {
                            totalPriceStatus: "FINAL",
                            totalPrice: "' . $order->info['total'] . '",
                            currencyCode: "' . $order->info['currency'] . '"
                        },
                        merchantInfo: {
                            merchantId: "' . $googleMerchantId . '",
                            merchantName: "' . STORE_NAME . '"
                        }
                    };

                    return paymentsClient.isReadyToPay({
                        apiVersion: 2,
                        apiVersionMinor: 0,
                        allowedPaymentMethods: paymentDataRequest.allowedPaymentMethods,
                        existingPaymentMethodRequired: true
                    }).then(function(response) {
                        if (response.result) {
                            var buttonContainer = document.getElementById("google-pay-button-container");
                            if (buttonContainer && !buttonContainer.hasChildNodes()) {
                                var button = paymentsClient.createButton({
                                    onClick: function(event) {
                                        event.preventDefault();
                                        paymentsClient.loadPaymentData(paymentDataRequest)
                                            .then(function(paymentData) {
                                                if (!paymentData || !paymentData.paymentMethodData) {
                                                    alert("Google Pay returned invalid payment data.");
                                                    return;
                                                }

                                                googlePayInstance.parseResponse(paymentData)
                                                    .then(function(tokenizedData) {
                                                        if (!tokenizedData.nonce) {
                                                            alert("Google Pay tokenization failed: No nonce returned.");
                                                            return;
                                                        }

                                                        var form = document.getElementById("checkout_payment");
                                                        var input = document.createElement("input");
                                                        input.setAttribute("type", "hidden");
                                                        input.setAttribute("name", "google_pay_response");
                                                        input.setAttribute("value", tokenizedData.nonce);
                                                        form.appendChild(input);
                                                        form.submit();
                                                    })
                                                    .catch(function(err) {
                                                        alert("Google Pay tokenization failed: " + err.message);
                                                    });
                                            })
                                            .catch(function(err) {
                                                alert("Google Pay load payment data error: " + err.message);
                                            });
                                    }
                                });

                                buttonContainer.appendChild(button);
                            }
                        }
                    }).catch(function(err) {
                        console.error("Error checking Google Pay availability", err);
                    });
                }).catch(function(error) {
                    console.error("Error during Google Pay setup", error);
                });
            }

            document.addEventListener("DOMContentLoaded", function() {
                initializeGooglePay();

                if (typeof updateForm === "function") {
                    console.log("Hooking into updateForm() to detect checkout updates.");

                    const originalUpdateForm = updateForm;
                    updateForm = function() {
                        console.log("updateForm() called - Checking for Google Pay button...");

                        originalUpdateForm.apply(this, arguments);

                        setTimeout(() => {
                            var buttonContainer = document.getElementById("google-pay-button-container");
                            if (!buttonContainer || buttonContainer.hasChildNodes()) {
                                console.log("Google Pay button already exists or container missing. Skipping reinitialization.");
                                return;
                            }

                            console.log("Google Pay button is missing, reinitializing...");
                            initializeGooglePay();
                        }, 250);
                    };
                } else {
                    console.warn("updateForm() not found; Google Pay reinitialization hook not attached.");
                }
            });
        </script>';

        $output .= '<div id="google-pay-button-container"></div>';

        return array(
            'id' => $this->code,
            'module' => MODULE_PAYMENT_BRAINTREE_GOOGLE_PAY_TEXT_ADMIN_TITLE,
            'fields' => array(array('title' => '', 'field' => $output))
        );
    }

    /**
     * Store the Google Pay token (nonce) and post it as a hidden input field
     */
    function process_button() {
        $process_button_string = "";

        if (!empty($_POST['google_pay_response'])) {
            $process_button_string .= "\n" . zen_draw_hidden_field('google_pay_nonce', $_POST['google_pay_response']);
        }

        return $process_button_string;
    }

    /**
     * Validate the Google Pay response and complete the payment
     */
    function before_process() {
        global $messageStack;

        if (empty($_POST['google_pay_nonce'])) {
            $messageStack->add_session('checkout_payment', MODULE_PAYMENT_BRAINTREE_GOOGLE_PAY_TEXT_PAYMENT_FAILED, 'error');
            zen_redirect(zen_href_link(FILENAME_CHECKOUT_PAYMENT, '', 'SSL', true, false));
        }

        $googlePayNonce = $_POST['google_pay_nonce'];
        $transaction_id = $this->process_braintree_payment($googlePayNonce, 'google_pay');

        if ($transaction_id) {
            $this->store_transaction_details($this->order_id, $transaction_id);
            return true;
        } else {
            $messageStack->add_session('checkout_payment', MODULE_PAYMENT_BRAINTREE_GOOGLE_PAY_TEXT_PAYMENT_FAILED, 'error');
            zen_redirect(zen_href_link(FILENAME_CHECKOUT_PAYMENT, '', 'SSL', true, false));
        }
    }

    /**
     * Process the Google Pay payment using the Braintree API
     */
    function process_braintree_payment($token, $method) {
        $gateway = $this->get_braintree_gateway(); // Get Braintree gateway instance

        // Get the configured Merchant Account ID (if available)
        $merchantAccountId = defined('MODULE_PAYMENT_BRAINTREE_GOOGLE_PAY_MERCHANT_ACCOUNT_ID') && !empty(MODULE_PAYMENT_BRAINTREE_GOOGLE_PAY_MERCHANT_ACCOUNT_ID)
            ? MODULE_PAYMENT_BRAINTREE_GOOGLE_PAY_MERCHANT_ACCOUNT_ID
            : null;

        try {
            $transactionData = [
                'amount' => $this->order->info['total'],
                'paymentMethodNonce' => $token,
                'options' => ['submitForSettlement' => true]
            ];

            // Only add the merchantAccountId if it's set in the configuration
            if (!is_null($merchantAccountId)) {
                $transactionData['merchantAccountId'] = $merchantAccountId;
            }

            $transaction = $gateway->transaction()->sale($transactionData);

            if ($transaction->success) {
                return $transaction->transaction->id; // Return transaction ID
            } else {
                return false;
            }
        } catch (Exception $e) {
            error_log('Braintree payment processing error: ' . $e->getMessage());
            return false;
        }
    }

    /**
     * Store transaction details after payment has been processed
     */
    function store_transaction_details($order_id, $transaction_id) {
        global $db;
        $sql = "INSERT INTO " . TABLE_BRAINTREE . " (order_id, txn_id, payment_type, payment_status, payment_date)
                VALUES ('" . (int)$order_id . "', '" . zen_db_input($transaction_id) . "', 'Google Pay', 'Completed', now())";
        $db->Execute($sql);
    }

    /**
     * Clean up after the order has been processed
     */
    function after_process() {
        // No additional processing needed
        return false;
    }

    /**
     * Check if the module is installed and active, and perform upgrades if necessary
     */
    function check() {
        global $db;

        if (!isset($this->_check)) {
            // Check if the module is enabled
            $check_query = $db->Execute("SELECT configuration_value FROM " . TABLE_CONFIGURATION . " WHERE configuration_key = 'MODULE_PAYMENT_BRAINTREE_GOOGLE_PAY_STATUS'");
            $this->_check = $check_query->RecordCount();
            if ($this->_check > 0 && $check_query->fields['configuration_value'] == 'True') {
                // If the module is enabled, run the upgrade process
                $this->upgrade();
            }
        }

        // Return whether the module is installed
        return $this->_check;
    }

    /**
     * Install the module and create required configuration entries
     */
    function install() {
        global $db;

        // Enable Google Pay configuration setting
        $db->Execute("INSERT INTO " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added)
                      VALUES ('Enable Google Pay', 'MODULE_PAYMENT_BRAINTREE_GOOGLE_PAY_STATUS', 'True', '" . MODULE_PAYMENT_BRAINTREE_GOOGLE_PAY_TEXT_ADMIN_DESCRIPTION . "', '6', '0', 'zen_cfg_select_option(array(\'True\', \'False\'), ', now())");

        $this->upgrade();
    }

    /**
     * Upgrade the module by adding missing configuration options
     */
    function upgrade() {
        global $db;

        // Check if the configuration key for Gateway Mode exists, if not, add it
        $check_query = $db->Execute("SELECT configuration_key FROM " . TABLE_CONFIGURATION . " WHERE configuration_key = 'MODULE_PAYMENT_BRAINTREE_GOOGLE_PAY_SERVER'");
        if ($check_query->RecordCount() == 0) {
            $db->Execute("INSERT INTO " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added)
                          VALUES ('Braintree Gateway Mode', 'MODULE_PAYMENT_BRAINTREE_GOOGLE_PAY_SERVER', 'sandbox', 'Set the Braintree environment (sandbox or production)', '6', '0', 'zen_cfg_select_option(array(\'sandbox\', \'production\'), ', now())");
        }

        // Check if the configuration key for Merchant ID exists, if not, add it
        $check_query_merchant_key = $db->Execute("SELECT configuration_key FROM " . TABLE_CONFIGURATION . " WHERE configuration_key = 'MODULE_PAYMENT_BRAINTREE_GOOGLE_PAY_MERCHANT_KEY'");
        if ($check_query_merchant_key->RecordCount() == 0) {
            $db->Execute("INSERT INTO " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added)
                          VALUES ('Braintree Merchant ID', 'MODULE_PAYMENT_BRAINTREE_GOOGLE_PAY_MERCHANT_KEY', '', 'Set your Braintree Merchant ID', '6', '0', now())");
        }

        // Check if the configuration key for Public Key exists, if not, add it
        $check_query_public_key = $db->Execute("SELECT configuration_key FROM " . TABLE_CONFIGURATION . " WHERE configuration_key = 'MODULE_PAYMENT_BRAINTREE_GOOGLE_PAY_PUBLIC_KEY'");
        if ($check_query_public_key->RecordCount() == 0) {
            $db->Execute("INSERT INTO " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added)
                          VALUES ('Braintree Public Key', 'MODULE_PAYMENT_BRAINTREE_GOOGLE_PAY_PUBLIC_KEY', '', 'Set your Braintree Public Key', '6', '0', now())");
        }

        // Check if the configuration key for Private Key exists, if not, add it
        $check_query_private_key = $db->Execute("SELECT configuration_key FROM " . TABLE_CONFIGURATION . " WHERE configuration_key = 'MODULE_PAYMENT_BRAINTREE_GOOGLE_PAY_PRIVATE_KEY'");
        if ($check_query_private_key->RecordCount() == 0) {
            $db->Execute("INSERT INTO " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added)
                          VALUES ('Braintree Private Key', 'MODULE_PAYMENT_BRAINTREE_GOOGLE_PAY_PRIVATE_KEY', '', 'Set your Braintree Private Key', '6', '0', now())");
        }

        // Check if the configuration key for Merchant Account ID exists, if not, add it
        $check_query_merchant_account_id = $db->Execute("SELECT configuration_key FROM " . TABLE_CONFIGURATION . " WHERE configuration_key = 'MODULE_PAYMENT_BRAINTREE_GOOGLE_PAY_MERCHANT_ACCOUNT_ID'");
        if ($check_query_merchant_account_id->RecordCount() == 0) {
            $db->Execute("INSERT INTO " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added)
                          VALUES ('Merchant Account ID', 'MODULE_PAYMENT_BRAINTREE_GOOGLE_PAY_MERCHANT_ACCOUNT_ID', '', 'Enter the Merchant Account ID linked to Braintree or leave blank to use the default account.', '6', '0', now())");
        }

        // Check if the configuration key for Tokenization Key exists, if not, add it
        $check_tokenization_key = $db->Execute("SELECT configuration_key FROM " . TABLE_CONFIGURATION . " WHERE configuration_key = 'MODULE_PAYMENT_BRAINTREE_GOOGLE_PAY_TOKENIZATION_KEY'");
        if ($check_tokenization_key->RecordCount() == 0) {
            $db->Execute("INSERT INTO " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added, set_function) VALUES ('Braintree Tokenization Key', 'MODULE_PAYMENT_BRAINTREE_GOOGLE_PAY_TOKENIZATION_KEY', '', 'Enter your Braintree Tokenization Key here. Found in Braintree API settings.', '6', '0', now(), '')");
        }

        // Check if the configuration key for Merchant ID exists, if not, add it
        $check_query_merchant_id = $db->Execute("SELECT configuration_key FROM " . TABLE_CONFIGURATION . " WHERE configuration_key = 'MODULE_PAYMENT_BRAINTREE_GOOGLE_PAY_MERCHANT_ID'");
        if ($check_query_merchant_id->RecordCount() == 0) {
            $db->Execute("INSERT INTO " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added)
                          VALUES ('Google Merchant ID', 'MODULE_PAYMENT_BRAINTREE_GOOGLE_PAY_MERCHANT_ID', '', 'Set your Google Merchant ID', '6', '0', now())");
        }

        // Check if the configuration key for Google Pay Environment exists, if not, add it
        $check_query_env = $db->Execute("SELECT configuration_key FROM " . TABLE_CONFIGURATION . " WHERE configuration_key = 'MODULE_PAYMENT_BRAINTREE_GOOGLE_PAY_ENVIRONMENT'");
        if ($check_query_env->RecordCount() == 0) {
            $db->Execute("INSERT INTO " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added)
                          VALUES ('Google Pay Environment', 'MODULE_PAYMENT_BRAINTREE_GOOGLE_PAY_ENVIRONMENT', 'TEST', 'Set your Google Pay environment (TEST or PRODUCTION)', '6', '0', 'zen_cfg_select_option(array(\'TEST\', \'PRODUCTION\'), ', now())");
        }

        // Add the missing ZONE and SORT_ORDER configuration
        $check_query_zone = $db->Execute("SELECT configuration_key FROM " . TABLE_CONFIGURATION . " WHERE configuration_key = 'MODULE_PAYMENT_BRAINTREE_GOOGLE_PAY_ZONE'");
        if ($check_query_zone->RecordCount() == 0) {
            $db->Execute("INSERT INTO " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, use_function, set_function, date_added)
                          VALUES ('Payment Zone', 'MODULE_PAYMENT_BRAINTREE_GOOGLE_PAY_ZONE', '0', 'If a zone is selected, only enable this payment method for that zone.', '6', '0', 'zen_get_zone_class_title', 'zen_cfg_pull_down_zone_classes(', now())");
        }

        $check_query_sort_order = $db->Execute("SELECT configuration_key FROM " . TABLE_CONFIGURATION . " WHERE configuration_key = 'MODULE_PAYMENT_BRAINTREE_GOOGLE_PAY_SORT_ORDER'");
        if ($check_query_sort_order->RecordCount() == 0) {
            $db->Execute("INSERT INTO " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added)
                          VALUES ('Sort Order', 'MODULE_PAYMENT_BRAINTREE_GOOGLE_PAY_SORT_ORDER', '0', 'Sort order of display.', '6', '0', now())");
        }

        // Ensure the "Braintree" table exists and is up-to-date
        $this->create_braintree_table();
    }


    /**
     * Ensure that the Braintree table exists and is up-to-date
     */
    function create_braintree_table() {
        global $db;

        // Check if the table exists
        $check_query = $db->Execute("SHOW TABLES LIKE '" . TABLE_BRAINTREE . "'");
        if ($check_query->RecordCount() == 0) {
            // If the table doesn't exist, create it
            $db->Execute("CREATE TABLE IF NOT EXISTS " . TABLE_BRAINTREE . " (
                id INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
                order_id INT(11) NOT NULL,
                txn_id VARCHAR(255) NOT NULL,
                payment_type VARCHAR(255) NOT NULL,
                payment_status VARCHAR(255) NOT NULL,
                payment_date DATETIME NOT NULL,
                amount DECIMAL(15,4) NOT NULL,
                currency VARCHAR(3) NOT NULL,
                customer_name VARCHAR(255) NOT NULL,
                customer_email VARCHAR(255) NOT NULL,
                google_pay_response TEXT,
                transaction_details TEXT,
                created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;");
        }
    }

    /**
     * Remove the module and its configuration entries
     */
    function remove() {
        global $db;
        $db->Execute("DELETE FROM " . TABLE_CONFIGURATION . " WHERE configuration_key IN ('" . implode("', '", $this->keys()) . "')");
    }

    /**
     * Return the configuration keys for this module
     */
    function keys() {
        return array(
            'MODULE_PAYMENT_BRAINTREE_GOOGLE_PAY_STATUS',
            'MODULE_PAYMENT_BRAINTREE_GOOGLE_PAY_SERVER',
            'MODULE_PAYMENT_BRAINTREE_GOOGLE_PAY_MERCHANT_KEY',
            'MODULE_PAYMENT_BRAINTREE_GOOGLE_PAY_PUBLIC_KEY',
            'MODULE_PAYMENT_BRAINTREE_GOOGLE_PAY_PRIVATE_KEY',
            'MODULE_PAYMENT_BRAINTREE_GOOGLE_PAY_TOKENIZATION_KEY',
            'MODULE_PAYMENT_BRAINTREE_GOOGLE_PAY_MERCHANT_ACCOUNT_ID',
            'MODULE_PAYMENT_BRAINTREE_GOOGLE_PAY_MERCHANT_ID',
            'MODULE_PAYMENT_BRAINTREE_GOOGLE_PAY_ENVIRONMENT',
            'MODULE_PAYMENT_BRAINTREE_GOOGLE_PAY_ZONE',
            'MODULE_PAYMENT_BRAINTREE_GOOGLE_PAY_SORT_ORDER'
        );
    }

    /**
     * Validate the form before submission when Google Pay is selected
     *
     * @return string JavaScript validation code
     */
    public function javascript_validation() {
        $js = '';

        // Check if Google Pay is selected and ensure it's properly validated
        $js .= '  if (payment_value == "' . $this->code . '") {' . "\n" .
               '    // Add any additional validation logic for Google Pay if necessary' . "\n" .
               '    return true;' . "\n" .
               '  }' . "\n";

        return $js;
    }
}
?>
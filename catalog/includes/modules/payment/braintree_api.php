<?php
// braintree_api.php payment module class
// needs to be loaded even in admin for edit orders
use Braintree\Gateway;
use Braintree\Transaction;

require_once(DIR_FS_CATALOG . DIR_WS_MODULES . 'payment/braintree/lib/Braintree.php');

if (!defined('TABLE_BRAINTREE')) define('TABLE_BRAINTREE', DB_PREFIX . 'braintree');

class braintree_api extends base {
    public $code;
    public $reasoncode;
    public $pendingreason;
    public $title;
    public $description;
    public $enabled;
    public $zone;
    public $cc_type_check = '';
    public $version = '';
    public $enableDebugging = false;
    public $sort_order = null;
    public $order_pending_status = 1;
    public $order_status = DEFAULT_ORDERS_STATUS_ID;
    public $_logLevel = 0;
    public $localEnvironment = true;
    public $codeVersion;
    public $emailAlerts;
    public $nonce;
    public $transaction_id;
    public $googleTransactionId;
    public $payment_type;
    public $payment_status;
    public $avs;
    public $cvv2;
    public $payment_time;
    public $amt;
    public $transactiontype;
    public $numitems;
    public $cc_card_type;
    public $cc_card_number;
    public $cc_expiry_month;
    public $cc_expiry_year;
    public $cc_checkcode;
    public $cards = [];

    /**
     * this module collects card-info onsite
     */
    //public $collectsCardDataOnsite = TRUE;

    /**
     * class constructor
     */
    function __construct() {

        $version = filter_var(PROJECT_VERSION_MINOR, FILTER_SANITIZE_NUMBER_FLOAT);
        if(PROJECT_VERSION_MAJOR > 1 && PROJECT_VERSION_MAJOR < 2 && $version < 5.8){
            include_once(zen_get_file_directory(DIR_FS_CATALOG . DIR_WS_LANGUAGES . $_SESSION['language'] . '/modules/payment/', 'braintree_api.php', 'false'));
        }

        global $order;

        $this->code = 'braintree_api';
        $this->title = MODULE_PAYMENT_BRAINTREE_TEXT_ADMIN_TITLE;
        $this->codeVersion = (defined('MODULE_PAYMENT_BRAINTREE_VERSION')) ? MODULE_PAYMENT_BRAINTREE_VERSION : false;
        $this->enabled = (defined('MODULE_PAYMENT_BRAINTREE_STATUS')) ? (MODULE_PAYMENT_BRAINTREE_STATUS == 'True') : false;

        // Set the title & description text based on the mode we're in
        if (IS_ADMIN_FLAG === true) {
            if (file_exists(DIR_FS_CATALOG . DIR_WS_FUNCTIONS . 'plugin_support.php')) {
                require_once(DIR_FS_CATALOG . DIR_WS_FUNCTIONS . 'plugin_support.php');
                $new_version_details = plugin_version_check_for_updates(1781, defined('MODULE_PAYMENT_BRAINTREE_VERSION') ? MODULE_PAYMENT_BRAINTREE_VERSION : '');
                if ($new_version_details !== FALSE) {
                    $this->title .= '<span class="alert">' . ' - NOTE: A NEW VERSION OF THIS PLUGIN IS AVAILABLE. <a href="' . $new_version_details['link'] . '" target="_blank">[Details]</a>' . '</span>';
                }
            }
            $this->description = sprintf(MODULE_PAYMENT_BRAINTREE_TEXT_ADMIN_DESCRIPTION, ($this->codeVersion) ? ' (rev' . $this->codeVersion . ')': '');
            $this->title = MODULE_PAYMENT_BRAINTREE_TEXT_ADMIN_TITLE;

            if ($this->enabled) {

                if (MODULE_PAYMENT_BRAINTREE_SERVER == 'sandbox')
                    $this->title .= '<strong><span class="alert"> (sandbox active)</span></strong>';
                if (((defined('MODULE_PAYMENT_BRAINTREE_DEBUGGING')) ? MODULE_PAYMENT_BRAINTREE_DEBUGGING : 'Alerts Only') == 'Log File' || ((defined('MODULE_PAYMENT_BRAINTREE_DEBUGGING')) ? MODULE_PAYMENT_BRAINTREE_DEBUGGING : 'Alerts Only') == 'Log and Email')
                    $this->title .= '<strong> (Debug)</strong>';
                if (!function_exists('curl_init'))
                    $this->title .= '<strong><span class="alert"> CURL NOT FOUND. Cannot Use.</span></strong>';
            }
        } else {

            $this->description = MODULE_PAYMENT_BRAINTREE_TEXT_DESCRIPTION;
            $this->title = MODULE_PAYMENT_BRAINTREE_TEXT_TITLE; //cc
        }

        if(isset($_SESSION['bt_payment_type']) && $_SESSION['bt_payment_type'] == 'AndroidPayCard'){
            $this->title = 'Braintree - Google Pay';
        }

        if(!$this->localEnvironment){
            if (
                (!defined('BRAINTREE_OVERRIDE_CURL_WARNING') ||
                    (defined('BRAINTREE_OVERRIDE_CURL_WARNING') &&
                    BRAINTREE_OVERRIDE_CURL_WARNING != 'True')
                )
                && !function_exists('curl_init'))
            {
                $this->enabled = false;
            }
        }


        $this->enableDebugging = (((defined('MODULE_PAYMENT_BRAINTREE_DEBUGGING')) ? MODULE_PAYMENT_BRAINTREE_DEBUGGING : 'Alerts Only') == 'Log File' || ((defined('MODULE_PAYMENT_BRAINTREE_DEBUGGING')) ? MODULE_PAYMENT_BRAINTREE_DEBUGGING : 'Alerts Only') == 'Log and Email');
        $this->emailAlerts = ((defined('MODULE_PAYMENT_BRAINTREE_DEBUGGING')) ? MODULE_PAYMENT_BRAINTREE_DEBUGGING : 'Alerts Only' == 'Log and Email');
        $this->sort_order = (defined('MODULE_PAYMENT_BRAINTREE_SORT_ORDER')) ? MODULE_PAYMENT_BRAINTREE_SORT_ORDER : null;
        $this->order_pending_status = (defined('MODULE_PAYMENT_BRAINTREE_ORDER_PENDING_STATUS_ID')) ? MODULE_PAYMENT_BRAINTREE_ORDER_PENDING_STATUS_ID : 1;

        if ((int) ((defined('MODULE_PAYMENT_BRAINTREE_ORDER_STATUS_ID')) ? MODULE_PAYMENT_BRAINTREE_ORDER_STATUS_ID : 1) > 0) {
            $this->order_status = (defined('MODULE_PAYMENT_BRAINTREE_ORDER_STATUS_ID')) ? MODULE_PAYMENT_BRAINTREE_ORDER_STATUS_ID : 1;
        }

        $this->zone = (int) (defined('MODULE_PAYMENT_BRAINTREE_ZONE')) ? MODULE_PAYMENT_BRAINTREE_ZONE : 0;

        if (is_object($order))
            $this->update_status();

        if (!(PROJECT_VERSION_MAJOR > 1 || (PROJECT_VERSION_MAJOR == 1 && substr(PROJECT_VERSION_MINOR, 0, 3) >= 5)))
            $this->enabled = false;

        // debug setup
        if (!defined('DIR_FS_LOGS')) {
            $log_dir = 'cache/';
        } else {
            $log_dir = DIR_FS_LOGS;
        }

        if (!@is_writable($log_dir))
            $log_dir = DIR_FS_CATALOG . $log_dir;
        if (!@is_writable($log_dir))
            $log_dir = DIR_FS_SQL_CACHE;
        // Regular mode:
        if ($this->enableDebugging)
            $this->_logLevel = 2;
        // DEV MODE:
        if (defined('BRAINTREE_DEV_MODE') && BRAINTREE_DEV_MODE == 'true')
            $this->_logLevel = 3;
    }

    /**
     *  Sets payment module status based on zone restrictions etc
     */
    function update_status() {
        global $order, $db;

        if(!$this->localEnvironment){
         // if store is not running in SSL, cannot offer credit card module, for PCI reasons
            if (IS_ADMIN_FLAG === false && (!defined('ENABLE_SSL') || ENABLE_SSL != 'true')) {
                $this->enabled = False;
                $this->zcLog('update_status', 'Module disabled because SSL is not enabled on this site.');
            }
        }

        // check other reasons for the module to be deactivated:
        if ($this->enabled && (int) $this->zone > 0) {

            $check_flag = false;

            $sql = "SELECT zone_id
                FROM " . TABLE_ZONES_TO_GEO_ZONES . "
                WHERE geo_zone_id = :zoneId
                AND zone_country_id = :countryId
                ORDER BY zone_id";

            $sql = $db->bindVars($sql, ':zoneId', $this->zone, 'integer');
            $sql = $db->bindVars($sql, ':countryId', $order->billing['country']['id'], 'integer');
            $check = $db->Execute($sql);

            while (!$check->EOF) {

                if ($check->fields['zone_id'] < 1) {
                    $check_flag = true;
                    break;
                } else if ($check->fields['zone_id'] == $order->billing['zone_id']) {
                    $check_flag = true;
                    break;
                }

                $check->MoveNext();
            }

            if (!$check_flag) {
                $this->enabled = false;
                $this->zcLog('update_status', 'Module disabled due to zone restriction. Billing address is not within the Payment Zone selected in the module settings.');
            }

            // module cannot be used for purchase > $10,000 USD
            $order_amount = $this->calc_order_amount($order->info['total'], 'USD');

            if ($order_amount > 10000) {
                $this->enabled = false;
                $this->zcLog('update_status', 'Module disabled because purchase price (' . $order_amount . ') exceeds Braintree-imposed maximum limit of 10,000 USD.');
            }

            if ($order->info['total'] == 0) {
                $this->enabled = false;
                /* $this->zcLog('update_status', 'Module disabled because purchase amount is set to 0.00.' . "\n" . print_r($order, true)); */
            }
        }
    }

    function javascript_validation(){
        return 'if(!braintreeCheck()) return false;';
    }

    /**
     * Display Credit Card Information Submission Fields on the Checkout Payment Page
     */
    function selection() {
        global $order;

        $this->cc_type_check = 'var value = document.checkout_payment.braintree_cc_type.value;' .
            'if(value == "Solo" || value == "Maestro" || value == "Switch") {' .
            '    document.checkout_payment.braintree_cc_issue_month.disabled = false;' .
            '    document.checkout_payment.braintree_cc_issue_year.disabled = false;' .
            '    document.checkout_payment.braintree_cc_checkcode.disabled = false;' .
            '    if(document.checkout_payment.braintree_cc_issuenumber) document.checkout_payment.braintree_cc_issuenumber.disabled = false;' .
            '} else {' .
            '    if(document.checkout_payment.braintree_cc_issuenumber) document.checkout_payment.braintree_cc_issuenumber.disabled = true;' .
            '    if(document.checkout_payment.braintree_cc_issue_month) document.checkout_payment.braintree_cc_issue_month.disabled = true;' .
            '    if(document.checkout_payment.braintree_cc_issue_year) document.checkout_payment.braintree_cc_issue_year.disabled = true;' .
            '    document.checkout_payment.braintree_cc_checkcode.disabled = false;' .
            '}';

        if (empty($this->cards)) {
            $this->cc_type_check = '';
        }

        $fieldsArray = [];

        if (!empty($this->cc_type_check)) {
            $fieldsArray[] = [
                "field" => '<script type="text/javascript">function braintree_cc_type_check() { ' . $this->cc_type_check . ' } </script>'
            ];
        }

        if (isset($this->cards) && is_countable($this->cards) && sizeof($this->cards) > 0) {
            $fieldsArray[] = [
                'title' => MODULE_PAYMENT_BRAINTREE_TEXT_CREDIT_CARD_TYPE,
                'field' => zen_draw_pull_down_menu(
                    'braintree_cc_type',
                    $this->cards,
                    '',
                    'onchange="braintree_cc_type_check();" onblur="braintree_cc_type_check();" id="' . $this->code . '-cc-type"'
                ),
                'tag' => $this->code . '-cc-type'
            ];
        }

        $fieldsArray[] = [
            'title' => MODULE_PAYMENT_BRAINTREE_TEXT_CREDIT_CARD_NUMBER,
            'field' => '<div id="braintree_api-cc-number-hosted"></div>'
        ];

        $fieldsArray[] = [
            'title' => MODULE_PAYMENT_BRAINTREE_TEXT_CREDIT_CARD_EXPIRES,
            'field' => '<div id="braintree_expiry-hosted"></div>'
        ];

        $fieldsArray[] = [
            'title' => MODULE_PAYMENT_BRAINTREE_TEXT_CREDIT_CARD_CHECKNUMBER,
            'field' => '<div id="braintree_api-cc-cvv-hosted"></div>'
        ];

        $clientToken = $this->gateway()->clientToken()->generate(['merchantAccountId' => MODULE_PAYMENT_BRAINTREE_MERCHANT_ACCOUNT_ID]);
        $fieldsArray[] = [
            'title' => "",
            'field' => "
                <input type=\"hidden\" name=\"braintree_nonce\" id=\"braintree_nonce\" />
                <script src='https://js.braintreegateway.com/web/3.115.2/js/client.min.js'></script>
                <script src='https://js.braintreegateway.com/web/3.115.2/js/three-d-secure.js'></script>
                <script src='https://js.braintreegateway.com/web/3.115.2/js/hosted-fields.js'></script>

                <script defer>
                    'use strict';

                    // Ensure hf and threeDS are globally accessible without redeclaring
                    if (typeof window.hf === 'undefined') {
                        window.hf = null;
                    }
                    if (typeof window.threeDS === 'undefined') {
                        window.threeDS = null;
                    }

                    function braintreeCheck() {
                        const selectedPayment = document.querySelector('#paymentModules [name=payment]:checked');
                        if (selectedPayment && selectedPayment.value === 'braintree_api' && document.getElementById('braintree_nonce').value === '') {
                            authorizeCard();
                            return false;
                        }
                        return true;
                    }

                    function authorizeCard() {
                        setLoading(\"loading\");
                        hf.tokenize().then(payload => {
                            return threeDS.verifyCard({
                                onLookupComplete: (data, next) => next(),
                                amount: \"" . number_format((float)$order->info['total'], 2, '.', '') . "\",
                                nonce: payload.nonce,
                                bin: payload.details.bin,
                                email: \"" . $order->customer['email_address'] . "\",
                                billingAddress: {
                                    givenName: \"" . $order->billing['firstname'] . "\",
                                    surname: \"" . $order->billing['lastname'] . "\",
                                    phoneNumber: \"" . $order->customer['telephone'] . "\",
                                    streetAddress: \"" . $order->billing['street_address'] . "\",
                                    locality: \"" . $order->billing['city'] . "\",
                                    region: \"" . $order->billing['state'] . "\",
                                    postalCode: \"" . $order->billing['postcode'] . "\",
                                    countryCodeAlpha2: \"" . $order->billing['country']['iso_code_2'] . "\",
                                }
                            });
                        }).then(payload => {
                            document.getElementById('braintree_nonce').value = payload.nonce;

                            setLoading(\"success\");

                            // Use your original form selector
                            const checkoutForm = document.querySelector('form[name=\"checkout_payment\"]');
                            if (checkoutForm) {
                                checkoutForm.submit();
                            } else {
                                console.error(\"Checkout form not found! Aborting submission.\");
                            }
                        }).catch(err => {
                            setLoading(\"reset\");
                            console.error(\"Error in authorizeCard:\", err);
                            alert(err.message);
                        });
                    }

                    function setLoading(type) {
                        const verifyBtn = document.querySelector('#paymentSubmit input');
                        if (!verifyBtn) return;

                        if (type === 'loading') {
                            verifyBtn.value = 'Processing Card...';
                            verifyBtn.disabled = true;
                        } else if (type === 'reset') {
                            verifyBtn.value = 'Continue';
                            verifyBtn.disabled = false;
                        } else if (type === 'success') {
                            verifyBtn.value = 'Card Authorized!';
                            verifyBtn.disabled = true;
                        }
                    }

                    document.addEventListener('DOMContentLoaded', () => {
                        if (typeof updateForm === 'function') {
                            console.log('Hooking into updateForm() to detect checkout updates.');

                            // Store the original updateForm function
                            const originalUpdateForm = updateForm;

                            // Override updateForm to include Braintree field reinitialization
                            updateForm = function () {
                                console.log('updateForm() called - Checking for Braintree fields...');

                                // Call the original function first
                                originalUpdateForm.apply(this, arguments);

                                // Wait a short moment to ensure content updates complete
                                setTimeout(() => {
                                    const braintreeFields = document.querySelector('.braintree_api'); // Check if Braintree fields exist
                                    const hostedFieldsExist = document.querySelector('.braintree-hosted-fields'); // Check if Hosted Fields exist inside

                                    if (!braintreeFields) {
                                        console.warn('Braintree container is missing after update.');
                                        return;
                                    }

                                    if (window.hf && hostedFieldsExist) {
                                        console.log('Braintree Hosted Fields are already active. Skipping reinitialization.');
                                        return;
                                    }

                                    console.log('Braintree fields are missing or inactive, reinitializing Hosted Fields...');
                                    resetBraintree().then(() => {
                                        if (typeof start === 'function') {
                                            start();
                                        }
                                    }).catch(err => console.error('Error resetting Braintree:', err));
                                }, 250); // Short delay to ensure the DOM is fully updated
                            };
                        } else {
                            console.warn('updateForm() not found; Braintree reinitialization hook not attached.');
                        }

                        // Initial Braintree setup on page load
                        resetBraintree().then(() => {
                            if (typeof start === 'function') {
                                start();
                            }
                        }).catch(err => console.error('Error resetting Braintree:', err));
                    });

                    /**
                     * Destroys existing Braintree Hosted Fields before reinitializing
                     */
                    function resetBraintree() {
                        return new Promise((resolve, reject) => {
                            if (window.hf && typeof window.hf.teardown === 'function') {
                                console.log(\"Destroying existing Braintree Hosted Fields instance...\");
                                window.hf.teardown().then(() => {
                                    window.hf = null; // Clear reference
                                    resolve(); // Resolve the promise after teardown completes
                                }).catch(err => {
                                    console.error(\"Error destroying Braintree instance:\", err);
                                    reject(err);
                                });
                            } else {
                                resolve(); // If there's no active instance, resolve immediately
                            }
                        });
                    }

                    function start() {
                        if (typeof braintree === 'undefined') {
                            if (!sessionStorage.getItem('braintree_reload')) {
                                sessionStorage.setItem('braintree_reload', 'true');
                                location.reload();
                            }
                        }
                        getClientToken();
                    }

                    function getClientToken() {
                        onFetchClientToken('$clientToken');
                    }

                    function onFetchClientToken(clientToken) {
                        return setupComponents(clientToken).then(instances => {
                            hf = instances[0];
                            threeDS = instances[1];
                        }).catch(err => console.error('component error:', err));
                    }

                    function setupComponents(clientToken) {
                        return Promise.all([
                            braintree.hostedFields.create({
                                authorization: clientToken,
                                fields: {
                                    number: { selector: '#braintree_api-cc-number-hosted', placeholder: '0000-0000-0000-0000' },
                                    cvv: { selector: '#braintree_api-cc-cvv-hosted', placeholder: '000' },
                                    expirationDate: { selector: '#braintree_expiry-hosted', placeholder: 'MM / YY' }
                                }
                            }),
                            braintree.threeDSecure.create({
                                authorization: clientToken,
                                version: 2
                            })
                        ]);
                    }
                </script>
            "
        ];

        return [
            'id' => $this->code,
            'module' => MODULE_PAYMENT_BRAINTREE_TEXT_TITLE,
            'fields' => $fieldsArray
        ];
    }

    function pre_confirmation_check(){///////////////////
        global $messageStack;
        if(empty($_POST['braintree_nonce'])){
            $messageStack->add_session($this->code, 'Must authorize card first', 'error');
            zen_redirect(zen_href_link(FILENAME_CHECKOUT_PAYMENT, '', 'SSL', true, false));
        }
        if(isset($_POST['bt_payment_type'])) $_SESSION['bt_payment_type'] = $_POST['bt_payment_type'];
        $this->nonce = $_POST['braintree_nonce'];
    }

    function confirmation(){
        $confirmation = ['title' => '', 'fields' => []];
        if(!empty($_POST['braintree_cc_firstname'])){
            $confirmation['fields'][] = [
                'title' => MODULE_PAYMENT_BRAINTREE_TEXT_CREDIT_CARD_FIRSTNAME,
                'field' => $_POST['braintree_cc_firstname']
            ];
        }
        if(!empty($_POST['braintree_cc_lastname'])){
            $confirmation['fields'][] = [
                'title' => MODULE_PAYMENT_BRAINTREE_TEXT_CREDIT_CARD_LASTNAME,
                'field' => $_POST['braintree_cc_lastname']
            ];
        }
        if(!empty($this->cc_card_type)){
            $confirmation['fields'][] = [
                'title' => MODULE_PAYMENT_BRAINTREE_TEXT_CREDIT_CARD_TYPE,
                'field' => $this->cc_card_type
            ];
        }
        if(!empty($_POST['braintree_cc_number'])){
            $cc_number = $_POST['braintree_cc_number'];
            if(strlen($cc_number) == 4){
                $cc_number = "XXXX-XXXX-XXXX-$cc_number";
            }else{
                $cc_number = substr($_POST['braintree_cc_number'], 0, 4) . str_repeat('X', (strlen($_POST['braintree_cc_number']) - 8)) . substr($_POST['braintree_cc_number'], -4);
            }
            $confirmation['fields'][] = [
                'title' => MODULE_PAYMENT_BRAINTREE_TEXT_CREDIT_CARD_NUMBER,
                'field' => $cc_number
            ];
        }
        if(!empty($_POST['braintree_cc_expires_month']) && !empty($_POST['braintree_cc_expires_year'])){
            $expiryMonth = $_POST['braintree_cc_expires_month'];
            $expiryYear = $_POST['braintree_cc_expires_year'];
            $confirmation['fields'][] = [
                'title' => MODULE_PAYMENT_BRAINTREE_TEXT_CREDIT_CARD_EXPIRES,
                'field' => "$expiryMonth/$expiryYear"
            ];
        }
        if(!empty($_POST['braintree_cc_issuenumber'])){
            $confirmation['fields'][] = [
                'title' => MODULE_PAYMENT_BRAINTREE_TEXT_ISSUE_NUMBER,
                'field' => $_POST['braintree_cc_issuenumber']
            ];
        }

        return $confirmation;
    }

    /**
     * Prepare the hidden fields comprising the parameters for the Submit button on the checkout confirmation page
     */
    function process_button() {
        global $order;

        $process_button_string = "";
        if(isset($_POST['braintree_cc_type'])) $process_button_string .= "\n" . zen_draw_hidden_field('bt_cc_type', $_POST['braintree_cc_type']);
        $process_button_string .= "\n" . zen_draw_hidden_field('bt_cc_expdate_month', $_POST['braintree_cc_expires_month']);
        $process_button_string .= "\n" . zen_draw_hidden_field('bt_cc_expdate_year', $_POST['braintree_cc_expires_year']);
        if(isset($_POST['braintree_cc_issue_month'])) $process_button_string .= "\n" . zen_draw_hidden_field('bt_cc_issuedate_month', $_POST['braintree_cc_issue_month']);
        if(isset($_POST['braintree_cc_issue_year'])) $process_button_string .= "\n" . zen_draw_hidden_field('bt_cc_issuedate_year', $_POST['braintree_cc_issue_year']);
        if(isset($_POST['braintree_cc_issuenumber'])) $process_button_string .= "\n" . zen_draw_hidden_field('bt_cc_issuenumber', $_POST['braintree_cc_issuenumber']);
        $process_button_string .= "\n" . zen_draw_hidden_field('bt_cc_number', $_POST['braintree_cc_number']);
        if(isset($_POST['braintree_cc_checkcode'])) $process_button_string .= "\n" . zen_draw_hidden_field('bt_cc_checkcode', $_POST['braintree_cc_checkcode']);
        if(isset($_POST['braintree_cc_firstname'])) $process_button_string .= "\n" . zen_draw_hidden_field('bt_payer_firstname', $_POST['braintree_cc_firstname']);
        if(isset($_POST['braintree_cc_lastname'])) $process_button_string .= "\n" . zen_draw_hidden_field('bt_payer_lastname', $_POST['braintree_cc_lastname']);
        $process_button_string .= "\n" . zen_draw_hidden_field('bt_nonce', $_POST['braintree_nonce']);
        $process_button_string .= "\n" . zen_draw_hidden_field('bt_liability_shift_possible', $_POST['braintree_liability_shift_possible']);
        $process_button_string .= "\n" . zen_draw_hidden_field('bt_liability_shifted', $_POST['braintree_liability_shifted']);
        $process_button_string .= "\n" . zen_draw_hidden_field('bt_action_code', $_POST['braintree_action_code']);
        $process_button_string .= "\n" . zen_draw_hidden_field('bt_cavv', $_POST['braintree_cavv']);
        $process_button_string .= "\n" . zen_draw_hidden_field('bt_currency_code', $_POST['braintree_currency_code']);
        $process_button_string .= "\n" . zen_draw_hidden_field('bt_eci_flag', $_POST['braintree_eci_flag']);
        $process_button_string .= "\n" . zen_draw_hidden_field('bt_pares_status', $_POST['braintree_pares_status']);
        $process_button_string .= "\n" . zen_draw_hidden_field('bt_signature_verification', $_POST['braintree_signature_verification']);
        $process_button_string .= "\n" . zen_draw_hidden_field('bt_verification_status', $_POST['braintree_verification_status']);
        $process_button_string .= "\n" . zen_draw_hidden_field('bt_3ds_auth_id', $_POST['braintree_3ds_auth_id']);
        $process_button_string .= "\n" . zen_draw_hidden_field('bt_card_type', $_POST['braintree_card_type']);
        $process_button_string .= zen_draw_hidden_field(zen_session_name(), zen_session_id());
        $process_button_string .= "\n" . zen_draw_hidden_field('bt_payment_type', $_POST['bt_payment_type']);

        return $process_button_string;
    }

    /**
     * Zen Cart 1.5.4 Prepare the hidden fields comprising the parameters for the Submit button on the checkout confirmation page
     */
    function process_button_ajax() {

        global $order;
        $processButton = array('ccFields' => array('bt_cc_type' => 'braintree_cc_type',
                'bt_cc_expdate_month' => 'braintree_cc_expires_month',
                'bt_cc_expdate_year' => 'braintree_cc_expires_year',
                'bt_cc_issuedate_month' => 'braintree_cc_issue_month',
                'bt_cc_issuedate_year' => 'braintree_cc_issue_year',
                'bt_cc_issuenumber' => 'braintree_cc_issuenumber',
                'bt_cc_number' => 'braintree_cc_number',
                'bt_cc_checkcode' => 'braintree_cc_checkcode',
                'bt_payer_firstname' => 'braintree_cc_firstname',
                'bt_payer_lastname' => 'braintree_cc_lastname',
                'bt_nonce' => 'braintree_nonce',
                'bt_liability_shift_possible' => 'braintree_liability_shift_possible',
                'bt_liability_shifted' => 'braintree_liability_shifted',
                'bt_action_code' => 'braintree_action_code',
                'bt_cavv' => 'braintree_cavv',
                'bt_currency_code' => 'braintree_currency_code',
                'bt_eci_flag' => 'braintree_eci_flag',
                'bt_pares_status' => 'braintree_pares_status',
                'bt_signature_verification' => 'braintree_signature_verification',
                'bt_verification_status' => 'braintree_verification_status',
                'bt_3ds_auth_id' => 'braintree_3ds_auth_id',
                'bt_card_type' => 'braintree_card_type',
                'bt_payment_type' => 'bt_payment_type'
            ), 'extraFields' => array(zen_session_name() => zen_session_id()));
        return $processButton;
    }

    /**
     * Prepare and submit the final authorization to Braintree via the appropriate means as configured
     */
    function before_process() {
        global $db, $order, $messageStack;

        //Custom_field
        $order_id_query = $db->Execute("SELECT AUTO_INCREMENT FROM information_schema.tables WHERE table_name = '". TABLE_ORDERS . "' AND table_schema = DATABASE( )");
        $order_id = 0;
        while(!$order_id_query->EOF){
            $order_id = $order_id_query->fields['AUTO_INCREMENT'];
            $order_id_query->MoveNext();
        }

        try{

            #region CC VALIDATION
            if(isset($_POST['bt_nonce'])){

            }
            else{
                $cc_data = $this->retrieveValidatedCCData();
                $order->info['cc_type']     = $cc_data->cc_type;
                $order->info['cc_number']   = substr($cc_data->cc_number, 0, 4) . str_repeat('X', (strlen($cc_data->cc_number) - 8)) . substr($cc_data->cc_number, -4);
                $order->info['cc_owner']    = $_SESSION['customer_first_name'] . ' ' . $_SESSION['customer_last_name'];
                $order->info['cc_expires']  = $cc_data->cc_expiry_month . substr($cc_data->cc_expiry_year, -2);
                $order->info['ip_address']  = current(explode(':', str_replace(',', ':', zen_get_ip_address())));
                $cc_checkcode = (is_numeric($_POST['bt_cc_checkcode']) ? $_POST['bt_cc_checkcode'] : 0);
            }

            #endregion

            #region PRODUCT LIST
            $products_list = "";
            for ($i = 0; $i < sizeof($order->products); $i++) {
                if ($products_list != "") {
                    $products_list .= "\n";
                }
                $current_products_id = explode(':', $order->products[$i]['id']);
                $products_list .= $order->products[$i]['qty'] . 'x' . $order->products[$i]['name'] . ' (' . $current_products_id[0] . ') ';
                if (isset($order->products[$i]['attributes']) && sizeof($order->products[$i]['attributes']) > 0) {
                    for ($j = 0, $n2 = sizeof($order->products[$i]['attributes']); $j < $n2; $j++) {
                        $products_list .= ' ' . $order->products[$i]['attributes'][$j]['value'];
                    }
                }
                $products_list .= ' $' . zen_round(zen_add_tax($order->products[$i]['final_price'], $order->products[$i]['tax']), 2);
            }
            $products_list = (strlen($products_list) > 255) ? substr($products_list, 0, 250) . ' ...' : $products_list;
            #endregion

            #region PROCESS SALE
            $setCurrency = defined("MODULE_PAYMENT_BRAINTREE_CURRENCY") ? MODULE_PAYMENT_BRAINTREE_CURRENCY : DEFAULT_CURRENCY;
            $sale_options = [
                "amount"                => $this->calc_order_amount($order->info['total'], $setCurrency),
                "customer"              => [
                    "firstName"         => $order->customer['firstname'],
                    "lastName"          => $order->customer['lastname'],
                    "phone"             => $order->customer['telephone'],
                    "email"             => $order->customer['email_address'],
                ],
                "billing"               => [
                    "firstName"         => $order->billing['firstname'],
                    "lastName"          => $order->billing['lastname'],
                    "streetAddress"     => $order->billing['street_address'],
                    "extendedAddress"   => $order->billing['suburb'],
                    "locality"          => $order->billing['city'],
                    "region"            => $order->billing['state'],
                    "postalCode"        => $order->billing['postcode'],
                    "countryCodeAlpha2" => $order->billing['country']['iso_code_2']
                ],
                "shipping"              => [
                    "firstName"         => $order->delivery['firstname'],
                    "lastName"          => $order->delivery['lastname'],
                    "streetAddress"     => $order->delivery['street_address'],
                    "extendedAddress"   => $order->delivery['suburb'],
                    "locality"          => $order->delivery['city'],
                    "region"            => $order->delivery['state'],
                    "postalCode"        => $order->delivery['postcode'],
                    "countryCodeAlpha2" => $order->delivery['country']['iso_code_2']
                ],
                "options"               => [
                    "submitForSettlement" => MODULE_PAYMENT_BRAINTREE_SETTLEMENT
                ],
                "customFields" => [
                    'order_id' => $order_id
                ]
            ];
            $sale_options["merchantAccountId"] = MODULE_PAYMENT_BRAINTREE_MERCHANT_ACCOUNT_ID;
            if(isset($_POST['bt_nonce'])){
                $sale_options["paymentMethodNonce"] = $_POST["bt_nonce"];
            }
            else{
                $sale_options["paymentMethodNonce"] = MODULE_PAYMENT_BRAINTREE_MERCHANT_ACCOUNT_ID;
                $sale_options["creditCard"] = [
                    "number"            => $cc_data->cc_number,
                    "expirationMonth"   => $cc_data->cc_expiry_month,
                    "expirationYear"    => $cc_data->cc_expiry_year,
                    "cardholderName"    => $order->billing['firstname'] . ' ' . $order->billing['lastname'],
                    "cvv"               => $cc_checkcode,
                ];
            }


            $result = $gateway = $this->gateway()->transaction()->sale($sale_options);

            if($result->success){
                $this->zcLog('before_process - DP-5', 'Result: Success');
                $this->transaction_id           = $result->transaction->id;
                if($result->transaction->paymentInstrumentType == 'android_pay_card'){
                    $type = 'Google Pay';
                    $this->googleTransactionId = $result->transaction->androidPayCard['googleTransactionId'];
                    // $_SESSION['bt_payment_type'] = $_POST['bt_payment_type'];
                } else {
                    $type = $result->transaction->creditCardDetails->cardType;
                }
                $this->payment_type             = MODULE_PAYMENT_BRAINTREE_TEXT_TITLE . '(' . $type . ')'; //MODULE_PAYMENT_BRAINTREE_TEXT_TITLE . '(' . $result->transaction->creditCardDetails->cardType . ')';
                $this->payment_status           = 'Completed';
                $this->avs                      = $result->transaction->avsPostalCodeResponseCode;
                $this->cvv2                     = $result->transaction->cvvResponseCode;
                $createdAt_date                 = new DateTime($result->transaction->createdAt->date); //$result->transaction->createdAt;
                $createdAt_formatted            = $createdAt_date->format('Y-m-d H:i:s');
                $this->payment_time             = $createdAt_formatted;
                $this->amt                      = $result->transaction->amount;
                $this->transactiontype          = 'cart';
                $this->numitems                 = sizeof($order->products);
                $_SESSION['bt_FIRSTNAME']       = $result->transaction->customerDetails->firstName;
                $_SESSION['bt_LASTNAME']        = $result->transaction->customerDetails->lastName;
                $_SESSION['bt_BUSINESS']        = $result->transaction->billingDetails->company;
                $_SESSION['bt_NAME']            = $result->transaction->creditCardDetails->cardholderName;
                $_SESSION['bt_SHIPTOSTREET']    = $result->transaction->shippingDetails->streetAddress;
                $_SESSION['bt_SHIPTOSTREET2']   = $result->transaction->shippingDetails->extendedAddress;
                $_SESSION['bt_SHIPTOCITY']      = $result->transaction->shippingDetails->locality;
                $_SESSION['bt_SHIPTOSTATE']     = $result->transaction->shippingDetails->region;
                $_SESSION['bt_SHIPTOZIP']       = $result->transaction->shippingDetails->postalCode;
                $_SESSION['bt_SHIPTOCOUNTRY']   = $result->transaction->shippingDetails->countryName;
                $_SESSION['bt_ORDERTIME']       = $this->payment_time;
                $_SESSION['bt_CURRENCY']        = $result->transaction->currencyIsoCode;
                $_SESSION['bt_AMT']             = $result->transaction->amount;
                $_SESSION['bt_EXCHANGERATE']    = $result->transaction->disbursementDetails->settlementCurrencyExchangeRate;
                $_SESSION['bt_EMAIL']           = $order->customer['email_address'];
                $_SESSION['bt_PARENTTRANSACTIONID'] = $result->transaction->refundId;

            }else if ($result->transaction) {
//                print_r("Error processing transaction:");
//                print_r("\n  message: " . $result->message);
//                print_r("\n  code: " . $result->transaction->processorResponseCode);
//                print_r("\n  text: " . $result->transaction->processorResponseText);
                $error_msg = 'Error processing transaction: ' . $result->message;
                if ($result->transaction->processorResponseCode != null && preg_match('/^1(\d+)/', $result->transaction->processorResponseCode)) {
                    // If it's a 1000 code it's Card Approved but since it didn't suceed above we assume it's Verification Failed.
                    // FROM " . TABLE_BRAINTREE . " : 1000 class codes mean the processor has successfully authorized the transaction; success will be true. However, the transaction could still be gateway rejected even though the processor successfully authorized the transaction if you have AVS and/or CVV rules set up and/or duplicate transaction checking is enabled and the transaction fails those validation.
                    $customer_error_msg = 'We were unable to process your credit card. Please make sure that your credit card and billing information is accurate and entered properly.';
                } else if ($result->transaction->processorResponseCode != null && preg_match('/^2(\d+)/', $result->transaction->processorResponseCode)) {
                    // If it's a 2000 code it's Card Declined
                    // FROM " . TABLE_BRAINTREE . " : 2000 class codes means the authorization was declined by the processor ; success will be false and the code is meant to tell you more about why the card was declined.
                    if (defined('BRAINTREE_ERROR_CODE_' . $result->transaction->processorResponseCode)) {
                        $customer_error_msg = constant('BRAINTREE_ERROR_CODE_' . $result->transaction->processorResponseCode);
                    } else {
                        $customer_error_msg = 'Processor Decline - Please try another card.';
                    }
                } else if ($result->transaction->processorResponseCode != null &&preg_match('/^3(\d+)/', $result->transaction->processorResponseCode)) {

                    // If it's a 3000 code it's a processor failure
                    // FROM " . TABLE_BRAINTREE . " : 3000 class codes are problems with the back-end processing network, and dont necessarily mean a problem with the card itself.

                    $customer_error_msg = 'Processor Network Unavailable - Try Again.';
                } else {

                    // This is the default error msg but technically it shouldn't be able to get here, Braintree in the future may add codes making it possible to not be a 1, 2, or 3k class code though.

                    $customer_error_msg = 'We were unable to process your credit card. Please make sure that your billing information is accurate and entered properly.';
                }

                $this->zcLog('before_process - DP-5', 'Result: ' . $error_msg);

                $detailedEmailMessage = MODULE_PAYMENT_BRAINTREE_TEXT_EMAIL_ERROR_MESSAGE . "\n\n" .
                    $result->message .
                    "\n\nProblem occurred while customer " .
                    //$order->customer['customer_id'] . ' -- ' . // Key doesn't exist in $order object
                    $order->customer['firstname'] . ' ' .
                    $order->customer['lastname'] . ' -- was attempting checkout.' . "\n\n" . 'Detailed Validation errors below: ' . "\n\n" .
                    'Code: ' . $result->transaction->processorResponseCode . ' text: ' . $result->transaction->processorResponseText;

                if ($this->emailAlerts)
                    zen_mail(STORE_NAME, STORE_OWNER_EMAIL_ADDRESS, MODULE_PAYMENT_BRAINTREE_TEXT_EMAIL_ERROR_SUBJECT, $detailedEmailMessage, STORE_OWNER, STORE_OWNER_EMAIL_ADDRESS, array('EMAIL_MESSAGE_HTML' => nl2br($detailedEmailMessage)), 'paymentalert');

                $messageStack->add_session('checkout_payment', $customer_error_msg, 'error');
                zen_redirect(zen_href_link(FILENAME_CHECKOUT_PAYMENT, '', 'SSL', true, false));
            } else {

            /* Examples

              print_r("Message: " . $result->message);
              print_r("\nValidation errors: \n");
              print_r($result->errors->deepAll());

             */

            $error_msg = 'Message: ' . $result->message;
            $detailed_error_msg = 'Message: ' . $result->message . ' Validation error(s): ' . $result->errors->deepAll();

            $this->zcLog('before_process - DP-5', 'Result: ' . $detailed_error_msg);

            $detailedEmailMessage = MODULE_PAYMENT_BRAINTREE_TEXT_EMAIL_ERROR_MESSAGE . "\n\n" .
                $result->message .
                "\n\nProblem occurred while customer " .
                //$order->customer['customer_id'] . ' -- ' . // Key doesn't exist in $order object
                $order->customer['firstname'] . ' ' .
                $order->customer['lastname'] . ' -- was attempting checkout.' . "\n\n" . 'Detailed Validation errors below: ' . "\n\n";

            foreach ($result->errors->deepAll() AS $error) {
                $detailedEmailMessage .= ($error->code . ": " . $error->message . "\n");
            }

            if ($this->emailAlerts)
                zen_mail(STORE_NAME, STORE_OWNER_EMAIL_ADDRESS, MODULE_PAYMENT_BRAINTREE_TEXT_EMAIL_ERROR_SUBJECT, $detailedEmailMessage, STORE_OWNER, STORE_OWNER_EMAIL_ADDRESS, array('EMAIL_MESSAGE_HTML' => nl2br($detailedEmailMessage)), 'paymentalert');

            $messageStack->add_session('checkout_payment', $error_msg, 'error');
            zen_redirect(zen_href_link(FILENAME_CHECKOUT_PAYMENT, '', 'SSL', true, false));
        }
            #endregion

        }catch (Exception $e){
            $this->zcLog('before_process - DP-5', 'Result: ' . $e->getMessage());
            $messageStack->add_session('checkout_payment', 'There was an error processing your order, please try again.', 'error');
            zen_redirect(zen_href_link(FILENAME_CHECKOUT_PAYMENT, '', 'SSL', true, false));
        }
    }

    /**
     * When the order returns from the processor, this stores the results in order-status-history and logs data for subsequent use
     */
    function after_process() {
        global $insert_id, $db, $order;

        // Add a new Order Status History record for this order's details
        $commentString = "Transaction ID: :transID: " .
                "\nPayment Type: :pmtType: " .
                ($this->payment_time != '' ? "\nTimestamp: :pmtTime: " : "") .
                "\nPayment Status: :pmtStatus: " .
                (isset($this->responsedata['auth_exp']) ? "\nAuth-Exp: " . $this->responsedata['auth_exp'] : "") .
                ($this->avs != '' ? "\nAVS Code: " . $this->avs . "\nCVV2 Code: " . $this->cvv2 : '') .
                (trim($this->amt) != '' ? "\nAmount: :orderAmt: " : "");

        $commentString = $db->bindVars($commentString, ':transID:', $this->transaction_id, 'noquotestring');
        $commentString = $db->bindVars($commentString, ':pmtType:', $this->payment_type, 'noquotestring');
        $commentString = $db->bindVars($commentString, ':pmtTime:', $this->payment_time, 'noquotestring');
        $commentString = $db->bindVars($commentString, ':pmtStatus:', $this->payment_status, 'noquotestring');
        $commentString = $db->bindVars($commentString, ':orderAmt:', $this->amt, 'noquotestring');

        $sql_data_array = array(array('fieldName' => 'orders_id', 'value' => $insert_id, 'type' => 'integer'),
            array('fieldName' => 'orders_status_id', 'value' => $order->info['order_status'], 'type' => 'integer'),
            array('fieldName' => 'date_added', 'value' => 'now()', 'type' => 'noquotestring'),
            array('fieldName' => 'customer_notified', 'value' => 0, 'type' => 'integer'),
            array('fieldName' => 'comments', 'value' => $commentString, 'type' => 'string'));

        $db->perform(TABLE_ORDERS_STATUS_HISTORY, $sql_data_array);

        // store the Braintree order meta data -- used for later matching and back-end processing activities
        $braintree_order = array('order_id' => $insert_id,
            'txn_type' => $this->transactiontype,
            'module_name' => $this->code,
            'module_mode' => 'USA',
            'reason_code' => $this->reasoncode,
            'payment_type' => $this->payment_type,
            'payment_status' => $this->payment_status,
            'pending_reason' => $this->pendingreason,
            'first_name' => $_SESSION['bt_FIRSTNAME'],
            'last_name' => $_SESSION['bt_LASTNAME'],
            'payer_business_name' => $_SESSION['bt_BUSINESS'],
            'address_name' => $_SESSION['bt_NAME'],
            'address_street' => $_SESSION['bt_SHIPTOSTREET'],
            'address_city' => $_SESSION['bt_SHIPTOCITY'],
            'address_state' => $_SESSION['bt_SHIPTOSTATE'],
            'address_zip' => $_SESSION['bt_SHIPTOZIP'],
            'address_country' => $_SESSION['bt_SHIPTOCOUNTRY'],
            'payer_email' => $_SESSION['bt_EMAIL'],
            'payment_date' => 'now()',
            'txn_id' => $this->transaction_id,
            'parent_txn_id' => $_SESSION['bt_PARENTTRANSACTIONID'],
            'num_cart_items' => (float) $this->numitems,
            'settle_amount' => (float) urldecode($_SESSION['bt_AMT']),
            'settle_currency' => $_SESSION['bt_CURRENCY'],
            'exchange_rate' => ($_SESSION['bt_EXCHANGERATE'] != null && urldecode($_SESSION['bt_EXCHANGERATE']) > 0 ? urldecode($_SESSION['bt_EXCHANGERATE']) : 1.0),
            'date_added' => 'now()'
        );

        zen_db_perform(TABLE_BRAINTREE, $braintree_order);
    }

    /**
     * Build admin-page components
     *
     * @param int $zf_order_id
     * @return string
     */
    function admin_notification($zf_order_id) {

        if (!defined('MODULE_PAYMENT_BRAINTREE_STATUS'))
            return '';
        global $db;

        $module = $this->code;
        $output = '';
        $response = $this->_GetTransactionDetails($zf_order_id);

        if (file_exists(DIR_FS_CATALOG . DIR_WS_MODULES . 'payment/braintree/braintree_admin_notification.php'))
            include_once(DIR_FS_CATALOG . DIR_WS_MODULES . 'payment/braintree/braintree_admin_notification.php');

        return $output;
    }

    /**
     * Used to read details of an existing transaction.  FOR FUTURE USE.
     */
    function _GetTransactionDetails($oID) {
        global $messageStack;
        $txnID = $this->getTransactionId($oID);
        $result = $this->gateway()->transaction()->find($txnID);
        try {
            // Load data into $response
            $response['FIRSTNAME'] = $result->customerDetails->firstName;
            $response['LASTNAME'] = $result->customerDetails->lastName;
            $response['BUSINESS'] = $result->billingDetails->company;
            $response['NAME'] = $result->creditCardDetails->cardholderName;
            $response['BILLTOSTREET'] = $result->billingDetails->streetAddress;
            $response['BILLTOSTREET2'] = $result->billingDetails->extendedAddress;
            $response['BILLTOCITY'] = $result->billingDetails->locality;
            $response['BILLTOSTATE'] = $result->billingDetails->region;
            $response['BILLTOZIP'] = $result->billingDetails->postalCode;
            $response['BILLTOCOUNTRY'] = $result->billingDetails->countryName;
            $response['TRANSACTIONID'] = $result->id;
            $response['PARENTTRANSACTIONID'] = $result->refundedTransactionId;
            $response['TRANSACTIONTYPE'] = $result->type;
            $response['PAYMENTTYPE'] = $result->creditCardDetails->cardType;
            $response['PAYMENTSTATUS'] = $result->status;

            $createdAt_date = new DateTime($result->createdAt->date); //new DateTime($result->createdAt->date ?? '');
            $createdAt_formatted = $createdAt_date->format('Y-m-d H:i:s');

            $response['ORDERTIME'] = $createdAt_formatted;

            $response['CURRENCY'] = $result->currencyIsoCode;
            $response['AMT'] = $result->amount;
            /** @var Braintree\DisbursementDetails $disbursementDetails */
            $disbursementDetails = $result->disbursementDetails;
            $response['EXCHANGERATE'] = $disbursementDetails->settlementCurrencyExchangeRate;
            $response['EMAIL'] = $result->customerDetails->email;
        } catch (Exception $e) {
            $messageStack->add($e->getMessage(), 'error');
        }

        return $response;
    }

    /**
     * Evaluate installation status of this module. Returns true if the status key is found.
     */
    function check() {
        global $db,$messageStack;

        if (!isset($this->_check)) {
            $check_query = $db->Execute("SELECT configuration_value FROM " . TABLE_CONFIGURATION . " WHERE configuration_key = 'MODULE_PAYMENT_BRAINTREE_STATUS'");
            $this->_check = !$check_query->EOF;
            if ($this->_check && defined('MODULE_PAYMENT_BRAINTREE_VERSION')) {
                $this->version = MODULE_PAYMENT_BRAINTREE_VERSION;
                // while $this->version must be same with switch default $this->version.
                // If not, loop will not be stopped until memory run out and it can cause issue with installation
                while ($this->version != '2.2.9') {
                    switch ($this->version) {
                        case '1.0.0':
                            $db->Execute("UPDATE " . TABLE_CONFIGURATION . " SET configuration_value = '1.0.1' WHERE configuration_key = 'MODULE_PAYMENT_BRAINTREE_VERSION' LIMIT 1;");
                            $db->Execute("CREATE TABLE IF NOT EXISTS " . TABLE_BRAINTREE . " (  `braintree_id` int(11) NOT NULL AUTO_INCREMENT,  `order_id` int(11) NOT NULL,  `txn_type` varchar(256) NOT NULL,  `module_name` text NOT NULL,  `reason_code` text NOT NULL,  `payment_type` varchar(256) NOT NULL,  `payment_status` varchar(256) NOT NULL,  `pending_reason` varchar(256) NOT NULL,  `first_name` text NOT NULL,  `last_name` text NOT NULL,  `payer_business_name` text NOT NULL,  `address_name` text NOT NULL,  `address_street` text NOT NULL,  `address_city` text NOT NULL,  `address_state` text NOT NULL,  `address_zip` varchar(256) NOT NULL,  `address_country` varchar(256) NOT NULL,  `payer_email` text NOT NULL,  `payment_date` date NOT NULL,  `txn_id` varchar(256) NOT NULL,  `parent_txn_id` varchar(256) NOT NULL,  `num_cart_items` int(11) NOT NULL,  `settle_amount` decimal(10,0) NOT NULL,  `settle_currency` varchar(256) NOT NULL,  `exchange_rate` decimal(10,0) NOT NULL,  `date_added` date NOT NULL,  `module_mode` text NOT NULL,  PRIMARY KEY (`braintree_id`),  UNIQUE KEY `order_id` (`order_id`)) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1");
                            $this->version = '1.0.1';
                            $messageStack->add('Updated Braintree Payments to v1.0.1', 'success');
                             // do not break and continue to the next version
                        case '1.0.1':
                            $db->Execute("UPDATE " . TABLE_CONFIGURATION . " SET configuration_value = '1.1.0' WHERE configuration_key = 'MODULE_PAYMENT_BRAINTREE_VERSION' LIMIT 1;");
                            $messageStack->add('Updated Braintree Payments to v1.1.0', 'success');
                            $this->version = '1.1.0';
                        case '1.1.0':
                            $db->Execute("UPDATE " . TABLE_CONFIGURATION . " SET configuration_value = '1.1.1' WHERE configuration_key = 'MODULE_PAYMENT_BRAINTREE_VERSION' LIMIT 1;");
                            $messageStack->add('Updated Braintree Payments to v1.1.1', 'success');
                            $this->version = '1.1.1';
                        case '1.1.1':
                            $db->Execute("INSERT INTO " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) VALUES ('Submit for Settlement', 'MODULE_PAYMENT_BRAINTREE_SETTLEMENT', 'true', 'Would you like to automatically Submit for Settlement?  Setting to false will only authorize and not submit for settlement (also know as capture) the transaction', '6', '14', 'zen_cfg_select_option(array(\'true\', \'false\'), ', now()) ON DUPLICATE KEY UPDATE configuration_key = configuration_key");
                            $db->Execute("UPDATE " . TABLE_CONFIGURATION . " SET configuration_value = '1.2.0' WHERE configuration_key = 'MODULE_PAYMENT_BRAINTREE_VERSION' LIMIT 1;");
                            $messageStack->add('Updated Braintree Payments to v1.2.0', 'success');
                            $this->version = '1.2.0';
                        case '1.2.0':
                            $db->Execute("UPDATE " . TABLE_CONFIGURATION . " SET configuration_value = '1.2.1' WHERE configuration_key = 'MODULE_PAYMENT_BRAINTREE_VERSION' LIMIT 1;");
                            $messageStack->add('Updated Braintree Payments to v1.2.1', 'success');
                            $this->version = '1.2.1';
                        case '1.2.1':
                            $db->Execute("UPDATE " . TABLE_CONFIGURATION . " SET configuration_value = '1.2.2' WHERE configuration_key = 'MODULE_PAYMENT_BRAINTREE_VERSION' LIMIT 1;");
                            $messageStack->add('Updated Braintree Payments to v1.2.2', 'success');
                            global $sniffer;

                            if ($sniffer->table_exists('braintree') && TABLE_BRAINTREE != 'braintree') {
                                $db->Execute("RENAME TABLE `braintree` TO `" . TABLE_BRAINTREE . "`");
                            }
                            $this->version = '1.2.2';
                        case '1.2.2':
                            $db->Execute("UPDATE " . TABLE_CONFIGURATION . " SET configuration_value = '1.3.0' WHERE configuration_key = 'MODULE_PAYMENT_BRAINTREE_VERSION' LIMIT 1;");
                            $messageStack->add('Updated Braintree Payments to v1.3.0', 'success');
                            $this->version = '1.3.0';
                        case '1.3.0':
                            $db->Execute("UPDATE " . TABLE_CONFIGURATION . " SET configuration_value = '1.3.1' WHERE configuration_key = 'MODULE_PAYMENT_BRAINTREE_VERSION' LIMIT 1;");
                            $messageStack->add('Updated Braintree Payments to v1.3.1', 'success');
                            $this->version = '1.3.1';
                        case '1.3.1':
                            $db->Execute("UPDATE " . TABLE_CONFIGURATION . " SET configuration_value = '1.3.2' WHERE configuration_key = 'MODULE_PAYMENT_BRAINTREE_VERSION' LIMIT 1;");
                            $messageStack->add('Updated Braintree Payments to v1.3.2', 'success');
                            $this->version = '1.3.2';
                        case '1.3.2':
                            $db->Execute("UPDATE " . TABLE_CONFIGURATION . " SET set_function = 'zen_cfg_select_option(array(\'Alerts Only\', \'Log File\', \'Log and Email\'), ', configuration_description = 'Would you like to enable debug mode?  A complete detailed log of failed transactions will be emailed to the store owner if Log and Email is selected.' WHERE configuration_key = 'MODULE_PAYMENT_BRAINTREE_DEBUGGING' LIMIT 1;");
                            $db->Execute("UPDATE " . TABLE_CONFIGURATION . " SET configuration_value = '1.3.3' WHERE configuration_key = 'MODULE_PAYMENT_BRAINTREE_VERSION' LIMIT 1;");
                            $messageStack->add('Updated Braintree Payments to v1.3.3', 'success');
                            $this->version = '1.3.3';
                        case '1.3.3':
                            $db->Execute("UPDATE " . TABLE_CONFIGURATION . " SET configuration_description = 'Your Merchant Account ID, this should contain your <strong>Merchant Account Name</strong>.<br>Example: myaccountUSD' WHERE configuration_key = 'MODULE_PAYMENT_BRAINTREE_MERCHANT_ACCOUNT_ID' LIMIT 1;");
                            $db->Execute("UPDATE " . TABLE_CONFIGURATION . " SET configuration_description = 'Your Merchant Account Settlement Currency, must be the same as currency code in your Merchant Account Name.<br> Example: USD, CAD, AUD - You can see your store currencies from the <a target=\"_blank\" href=\"currencies.php\">Localization/Currency</a>(Opens New Window).' WHERE configuration_key = 'MODULE_PAYMENT_BRAINTREE_CURRENCY' LIMIT 1;");
                            $db->Execute("UPDATE " . TABLE_CONFIGURATION . " SET configuration_value = '1.3.4', configuration_description = 'Version installed', set_function = 'zen_cfg_select_option(array(\'1.3.4\'), ' WHERE configuration_key = 'MODULE_PAYMENT_BRAINTREE_VERSION' LIMIT 1;");
                            $messageStack->add('Updated Braintree Payments to v1.3.4', 'success');
                            $this->version = '1.3.4';
                        case '1.3.4':
                            $db->Execute("UPDATE " . TABLE_CONFIGURATION . " SET configuration_value = '1.3.5', configuration_description = 'Version installed', set_function = 'zen_cfg_select_option(array(\'1.3.5\'), ' WHERE configuration_key = 'MODULE_PAYMENT_BRAINTREE_VERSION' LIMIT 1;");
                            $messageStack->add('Updated Braintree Payments to v1.3.5', 'success');
                            $this->version = '1.3.5';
                        case '1.3.5':
                            $db->Execute("UPDATE " . TABLE_CONFIGURATION . " SET configuration_value = '1.4.0', configuration_description = 'Version installed', set_function = 'zen_cfg_select_option(array(\'1.4.0\'), ' WHERE configuration_key = 'MODULE_PAYMENT_BRAINTREE_VERSION' LIMIT 1;");
                            $messageStack->add('Updated Braintree Payments to v1.4.0', 'success');
                            $this->version = '1.4.0';
                        case '1.4.0':
                        case '1.4.1':
                        case '1.4.2':
                        case '1.4.3':
                        case '2.0.0':
                        case '2.1.0':
                        case '2.2.0':
                        case '2.2.1':
                        case '2.2.2':
                        case '2.2.3':
                        case '2.2.4':
                        case '2.2.5':
                        case '2.2.6':
                        case '2.2.7':
                        case '2.2.8':
                            $db->Execute("UPDATE " . TABLE_CONFIGURATION . " SET configuration_value = '2.2.9', configuration_description = 'Version installed', set_function = 'zen_cfg_select_option(array(\'2.2.9\'), ' WHERE configuration_key = 'MODULE_PAYMENT_BRAINTREE_VERSION' LIMIT 1;");
                            $messageStack->add('Updated Braintree Payments to v2.2.9', 'success');
                            $this->version = '2.2.9';
                        default:
                            $this->version = '2.2.9';
                            // break all the loops
                            break; // this break should only appear on the last case
                    }
                }
            }
        }

        return $this->_check;
    }

    /**
     * Installs all the configuration keys for this module
     */
    function install() {
        global $db, $messageStack;

        if (defined('MODULE_PAYMENT_BRAINTREE_STATUS')) {
            $messageStack->add_session('Braintree module already installed.', 'error');
            zen_redirect(zen_href_link(FILENAME_MODULES, 'set=payment&module=braintree', 'NONSSL'));
            return 'failed';
        }
        // Turn off module on fresh install to avoid issue on checkout page.
        $db->Execute("INSERT INTO " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) VALUES ('Enable this Payment Module', 'MODULE_PAYMENT_BRAINTREE_STATUS', 'False', 'Do you want to enable this payment module?', '6', '1', 'zen_cfg_select_option(array(\'True\', \'False\'), ', now())");
        $db->Execute("INSERT INTO " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) VALUES ('Version', 'MODULE_PAYMENT_BRAINTREE_VERSION', '2.2.9', 'Version installed', '6', '2', 'zen_cfg_select_option(array(\'2.2.9\'), ', now())");
        $db->Execute("INSERT INTO " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) VALUES ('Merchant Key', 'MODULE_PAYMENT_BRAINTREE_MERCHANTID', '', 'Your Merchant ID provided under the API Keys section.', '6', '3', now())");
        $db->Execute("INSERT INTO " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) VALUES ('Public Key', 'MODULE_PAYMENT_BRAINTREE_PUBLICKEY', '', 'Your Public Key provided under the API Keys section.', '6', '4', now())");
        $db->Execute("INSERT INTO " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) VALUES ('Private Key', 'MODULE_PAYMENT_BRAINTREE_PRIVATEKEY', '', 'Your Private Key provided under the API Keys section.', '6', '5', now())");
        $db->Execute("INSERT INTO " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) VALUES ('Merchant Account ID', 'MODULE_PAYMENT_BRAINTREE_MERCHANT_ACCOUNT_ID', '', 'Your Merchant Account ID, this should contain your <strong>Merchant Account Name</strong>.<br>Example: myaccountUSD', '6', '6', now())");
        $db->Execute("INSERT INTO " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) VALUES ('Production or Sandbox', 'MODULE_PAYMENT_BRAINTREE_SERVER', 'sandbox', '<strong>Production: </strong> Used to process Live transactions<br><strong>Sandbox: </strong>For developers and testing', '6', '7', 'zen_cfg_select_option(array(\'production\', \'sandbox\'), ', now())");
        $db->Execute("INSERT INTO " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) VALUES ('Merchant Account Default Currency', 'MODULE_PAYMENT_BRAINTREE_CURRENCY', 'USD', 'Your Merchant Account Settlement Currency, must be the same as currency code in your Merchant Account Name.<br> Example: USD, CAD, AUD - You can see your store currencies from the <a target=\"_blank\" href=\"currencies.php\">Localization/Currency</a>(Opens New Window).', '6', '8', now())");
        $db->Execute("INSERT INTO " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) VALUES ('Sort order of display.', 'MODULE_PAYMENT_BRAINTREE_SORT_ORDER', '0', 'Sort order of display. Lowest is displayed first.', '6', '9', now())");
        $db->Execute("INSERT INTO " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, use_function, set_function, date_added) VALUES ('Payment Zone', 'MODULE_PAYMENT_BRAINTREE_ZONE', '0', 'If a zone is selected, only enable this payment method for that zone.', '6', '10', 'zen_get_zone_class_title', 'zen_cfg_pull_down_zone_classes(', now())");
        $db->Execute("INSERT INTO " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, use_function, date_added) VALUES ('Set Order Status', 'MODULE_PAYMENT_BRAINTREE_ORDER_STATUS_ID', '2', 'Set the status of orders paid with this payment module to this value. <br /><strong>Recommended: Processing[2]</strong>', '6', '11', 'zen_cfg_pull_down_order_statuses(', 'zen_get_order_status_name', now())");
        $db->Execute("INSERT INTO " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, use_function, date_added) VALUES ('Set Unpaid Order Status', 'MODULE_PAYMENT_BRAINTREE_ORDER_PENDING_STATUS_ID', '1', 'Set the status of unpaid orders made with this payment module to this value. <br /><strong>Recommended: Pending[1]</strong>', '6', '12', 'zen_cfg_pull_down_order_statuses(', 'zen_get_order_status_name', now())");
        $db->Execute("INSERT INTO " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, use_function, date_added) VALUES ('Set Refund Order Status', 'MODULE_PAYMENT_BRAINTREE_REFUNDED_STATUS_ID', '1', 'Set the status of refunded orders to this value. <br /><strong>Recommended: Pending[1]</strong>', '6', '13', 'zen_cfg_pull_down_order_statuses(', 'zen_get_order_status_name', now())");
        $db->Execute("INSERT INTO " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) VALUES ('Debug Mode', 'MODULE_PAYMENT_BRAINTREE_DEBUGGING', 'Alerts Only', 'Would you like to enable debug mode?  A complete detailed log of failed transactions will be emailed to the store owner if Log and Email is selected.', '6', '20', 'zen_cfg_select_option(array(\'Alerts Only\', \'Log File\', \'Log and Email\'), ', now())");
        $db->Execute("INSERT INTO " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) VALUES ('Submit for Settlement', 'MODULE_PAYMENT_BRAINTREE_SETTLEMENT', 'true', 'Would you like to automatically Submit for Settlement?  Setting to false will only authorize and not submit for settlement (also know as capture) the transaction', '6', '14', 'zen_cfg_select_option(array(\'true\', \'false\'), ', now())");
        $db->Execute("CREATE TABLE IF NOT EXISTS " . TABLE_BRAINTREE . " (  `braintree_id` int(11) NOT NULL AUTO_INCREMENT,  `order_id` int(11) NOT NULL,  `txn_type` varchar(256) NOT NULL,  `module_name` text NOT NULL,  `reason_code` text NOT NULL,  `payment_type` varchar(256) NOT NULL,  `payment_status` varchar(256) NOT NULL,  `pending_reason` varchar(256) NOT NULL,  `first_name` text NOT NULL,  `last_name` text NOT NULL,  `payer_business_name` text NOT NULL,  `address_name` text NOT NULL,  `address_street` text NOT NULL,  `address_city` text NOT NULL,  `address_state` text NOT NULL,  `address_zip` varchar(256) NOT NULL,  `address_country` varchar(256) NOT NULL,  `payer_email` text NOT NULL,  `payment_date` date NOT NULL,  `txn_id` varchar(256) NOT NULL,  `parent_txn_id` varchar(256) NOT NULL,  `num_cart_items` int(11) NOT NULL,  `settle_amount` decimal(10,0) NOT NULL,  `settle_currency` varchar(256) NOT NULL,  `exchange_rate` decimal(10,0) NOT NULL,  `date_added` date NOT NULL,  `module_mode` text NOT NULL,  PRIMARY KEY (`braintree_id`),  UNIQUE KEY `order_id` (`order_id`)) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1");
        $this->notify('NOTIFY_PAYMENT_BRAINTREE_INSTALLED');
    }

    function keys() {

        $keys_list = array(
            'MODULE_PAYMENT_BRAINTREE_STATUS',
            'MODULE_PAYMENT_BRAINTREE_VERSION',
            'MODULE_PAYMENT_BRAINTREE_MERCHANTID',
            'MODULE_PAYMENT_BRAINTREE_PUBLICKEY',
            'MODULE_PAYMENT_BRAINTREE_PRIVATEKEY',
            'MODULE_PAYMENT_BRAINTREE_CURRENCY',
            'MODULE_PAYMENT_BRAINTREE_SORT_ORDER',
            'MODULE_PAYMENT_BRAINTREE_ZONE',
            'MODULE_PAYMENT_BRAINTREE_ORDER_STATUS_ID',
            'MODULE_PAYMENT_BRAINTREE_ORDER_PENDING_STATUS_ID',
            'MODULE_PAYMENT_BRAINTREE_REFUNDED_STATUS_ID',
            'MODULE_PAYMENT_BRAINTREE_SERVER',
            'MODULE_PAYMENT_BRAINTREE_DEBUGGING',
            'MODULE_PAYMENT_BRAINTREE_MERCHANT_ACCOUNT_ID',
            'MODULE_PAYMENT_BRAINTREE_SETTLEMENT'
        );

        return $keys_list;
    }

    /**
     * Uninstall this module
     */
    function remove() {
        global $db;

        $db->Execute("DELETE FROM " . TABLE_CONFIGURATION . " WHERE configuration_key LIKE 'MODULE\_PAYMENT\_BRAINTREE\_%'");
        $this->notify('NOTIFY_PAYMENT_BRAINTREE_UNINSTALLED');
    }

    /**
     * Debug Logging support
     */
    function zcLog($stage, $message) {
        static $tokenHash;

        if ($tokenHash == '')
            $tokenHash = '_' . zen_create_random_value(4);

        if (MODULE_PAYMENT_BRAINTREE_DEBUGGING == 'Log and Email' || MODULE_PAYMENT_BRAINTREE_DEBUGGING == 'Log File') {

            $token = date('m-d-Y-H-i');
            $token .= $tokenHash;
            if (!defined('DIR_FS_LOGS')) {
                $log_dir = 'cache/';
            } else {
                $log_dir = DIR_FS_LOGS;
            }
            $file = $log_dir . '/' . $this->code . '_Braintree_Action_' . $token . '.log';
            if (defined('BRAINTREE_DEV_MODE') && BRAINTREE_DEV_MODE == 'true')
                $file = $log_dir . '/' . $this->code . '_Braintree_Debug_' . $token . '.log';
            $fp = @fopen($file, 'a');
            @fwrite($fp, date('M-d-Y H:i:s') . ' (' . time() . ')' . "\n" . $stage . "\n" . $message . "\n=================================\n\n");
            @fclose($fp);
        }

        $this->_doDebug($stage, $message, false);
    }

    /**
     * @param $oID
     * @param string $amount
     * @param string $note
     * @return bool
     *
     *  Used to submit a refund for a given transaction.
     */
    function _doRefund($oID, $amount = 'Full', $note = '') {
        global $db, $messageStack;
        try{

            #region VARIABLES AND CHECKS

            $new_order_status = (int) MODULE_PAYMENT_BRAINTREE_REFUNDED_STATUS_ID;
            $refundNote = strip_tags(zen_db_input($_POST['refnote']));
            $partialRefund = 0;
            $txnID = $this->getTransactionId($oID);

            if (isset($_POST['partialrefund']) && $_POST['partialrefund'] == MODULE_PAYMENT_BRAINTREE_ENTRY_REFUND_BUTTON_TEXT_PARTIAL) {
                $partialRefund = (float)$_POST['refamt'];
                if ($partialRefund <= 0) {
                    throw new Exception(MODULE_PAYMENT_BRAINTREE_TEXT_INVALID_REFUND_AMOUNT);
                }
            }
            else if(isset($_POST['fullrefund']) && $_POST['fullrefund'] == MODULE_PAYMENT_BRAINTREE_ENTRY_REFUND_BUTTON_TEXT_FULL){
                if(!isset($_POST["reffullconfirm"]) || (isset($_POST['reffullconfirm']) && $_POST['reffullconfirm'] != 'on')){
                    throw new Exception(MODULE_PAYMENT_BRAINTREE_TEXT_REFUND_FULL_CONFIRM_ERROR);
                }
            }
            else{
                throw new Exception("Unable to proceed to Refund");
            }

            $brainTreeTxn = $this->gateway()->transaction()->find($txnID);

            #endregion

            #region DO REFUND/VOID

            if (
                $brainTreeTxn->status == Transaction::SUBMITTED_FOR_SETTLEMENT ||
                $brainTreeTxn->status == Transaction::AUTHORIZED)
            {
                $result = $this->gateway()->transaction()->void($txnID);
            }
            else if (
                $brainTreeTxn->status == Transaction::SETTLED ||
                $brainTreeTxn->status == Transaction::SETTLING
            ){
                if($partialRefund > 0){
                    $result = $this->gateway()->transaction()->refund($txnID,$partialRefund);
                }else{
                    $result = $this->gateway()->transaction()->refund($txnID);
                }
            }
            else{
                throw new Exception("Invalid status for Refund or Void");
            }

            #endregion

            #region UPDATE ORDER RECORD
            if ($result->success) {

                if(!empty($result->refundIds) && is_array($result->refundIds)){
                    $refundId = implode(",",$result->refundIds);
                }
                else if(!empty($result->refundId)){
                    $refundId = $result->refundId;
                }
                else{
                    $refundId = $txnID;
                }
                $final_refund_amount = !empty( $result->transaction->amount ) ?  $result->transaction->amount : $partialRefund;
                $new_order_status = ($new_order_status > 0 ? $new_order_status : 1);

                $sql_data_array = array('orders_id' => $oID,
                    'orders_status_id' => (int) $new_order_status,
                    'date_added' => 'now()',
                    'comments' => 'REFUND INITIATED. Trans ID:' . $refundId . "\n" . ' Gross Refund Amt: ' . $final_refund_amount . "\n" . $refundNote,
                    'customer_notified' => 0
                );
                zen_db_perform(TABLE_ORDERS_STATUS_HISTORY, $sql_data_array);

                $db->Execute("UPDATE " . TABLE_ORDERS . "
                        SET orders_status = '" . (int) $new_order_status . "'
                        WHERE orders_id = '" . (int) $oID . "'");

                $messageStack->add_session(sprintf(MODULE_PAYMENT_BRAINTREE_TEXT_REFUND_INITIATED, $final_refund_amount, $refundId), 'success');
            } else {
                $errorMessage = '';
                foreach ($result->errors->deepAll() AS $error) {
                    $errorMessage .= ($error->code . ": " . $error->message . "<br>");
                }
                throw new Exception($errorMessage);
            }
            #endregion

            return true;
        }catch (Exception $e){
            $messageStack->add_session($e->getMessage(), 'error');
            return false;
        }
    }

    /**
     * Debug Emailing support
     */
    function _doDebug($subject = 'Braintree debug data', $data = '', $useSession = true) {

        if (MODULE_PAYMENT_BRAINTREE_DEBUGGING == 'Log and Email') {

            $data = urldecode($data) . "\n\n";
            if ($useSession)
                $data .= "\nSession data: " . print_r($_SESSION, true);
            zen_mail(STORE_NAME, STORE_OWNER_EMAIL_ADDRESS, $subject, $this->code . "\n" . $data, STORE_OWNER, STORE_OWNER_EMAIL_ADDRESS, array('EMAIL_MESSAGE_HTML' => nl2br($this->code . "\n" . $data)), 'debug');
        }
    }

    /**
     * Initialize the Braintree object for communication to the processing gateways
     */
    function braintree_init() {

        if (MODULE_PAYMENT_BRAINTREE_MERCHANTID != '' && MODULE_PAYMENT_BRAINTREE_PUBLICKEY != '' && MODULE_PAYMENT_BRAINTREE_PRIVATEKEY != '') {
            Braintree\Configuration::environment(MODULE_PAYMENT_BRAINTREE_SERVER);
            Braintree\Configuration::merchantId(MODULE_PAYMENT_BRAINTREE_MERCHANTID);
            Braintree\Configuration::publicKey(MODULE_PAYMENT_BRAINTREE_PUBLICKEY);
            Braintree\Configuration::privateKey(MODULE_PAYMENT_BRAINTREE_PRIVATEKEY);
        } else {
            return FALSE;
        }
    }

    /**
     * Calculate the amount based on acceptable currencies
     */
    function calc_order_amount($amount, $braintreeCurrency, $applyFormatting = false) {
        global $currencies;

        $amount = ($amount * $currencies->get_value($braintreeCurrency));

        if ($braintreeCurrency == 'JPY' || (int) $currencies->get_decimal_places($braintreeCurrency) == 0) {
            $amount = (int) $amount;
            $applyFormatting = FALSE;
        }

        $amount = round($amount, 2);

        return ($applyFormatting ? number_format($amount, $currencies->get_decimal_places($braintreeCurrency)) : $amount);
    }

    /**
     * @return Gateway
     * @throws \Braintree\Exception
     */
    public function gateway(){
        global $messageStack;
        if(
            !defined("MODULE_PAYMENT_BRAINTREE_SERVER") ||
            !defined("MODULE_PAYMENT_BRAINTREE_MERCHANTID") ||
            !defined("MODULE_PAYMENT_BRAINTREE_PUBLICKEY") ||
            !defined("MODULE_PAYMENT_BRAINTREE_PRIVATEKEY")
        ){
            $messageStack->add_session("Braintree configuration not set!", 'error');
        }
        return new Braintree\Gateway([
            'environment' => MODULE_PAYMENT_BRAINTREE_SERVER,
            'merchantId' => MODULE_PAYMENT_BRAINTREE_MERCHANTID,
            'publicKey' => MODULE_PAYMENT_BRAINTREE_PUBLICKEY,
            'privateKey' => MODULE_PAYMENT_BRAINTREE_PRIVATEKEY
        ]);
    }

    /**
     * @param $order_id
     * @return string
     * @throws Exception
     */
    public function getTransactionId($order_id){
        global $db;

        if(empty($order_id) || $order_id <= 0){
            throw new Exception("Order ID is not valid");
        }

        $sql = "SELECT * FROM " . TABLE_BRAINTREE . "  WHERE order_id = :orderID AND parent_txn_id = '' ";
        $sql = $db->bindVars($sql, ':orderID', $order_id, 'integer');
        $zc_btHist = $db->Execute($sql);

        if ($zc_btHist->RecordCount() == 0){
            throw new Exception("Record is not found with ID:$order_id");
        }

        if(empty($zc_btHist->fields['txn_id'])){
            throw new Exception("Transaction ID is not found for this Order");
        }

        return $zc_btHist->fields['txn_id'];
    }

    /**
     * @param $array
     * @param $prop_name
     * @return string
     * @throws Exception
     */
    private function checkGetValue($array,$prop_name){
        if(!is_array($array)){
            throw new Exception("Key array is expected");
        }
        if(!is_string($prop_name) || empty($prop_name)){
            throw new Exception("Invalid property name type");
        }
        if(!isset($array[$prop_name])){
            throw new Exception("Property $prop_name does not exist");
        }
        return $array[$prop_name];
    }

    /**
     * @return cc_validation
     * @throws Exception
     */
    private function retrieveValidatedCCData(){
        global $messageStack;
        include(DIR_WS_CLASSES . 'cc_validation.php');

        $cc_number = $this->checkGetValue($_POST,"bt_cc_number");
        $cc_expiry_month = $this->checkGetValue($_POST,"bt_cc_expdate_month");
        $cc_expiry_year = $this->checkGetValue($_POST,"bt_cc_expdate_year");
        $cc_issue_month = $this->checkGetValue($_POST,"bt_cc_issuedate_month");
        $cc_issue_year = $this->checkGetValue($_POST,"bt_cc_issuedate_year");

        $cc_validation = new cc_validation();
        $check_result = $cc_validation->validate($cc_number,$cc_expiry_month,$cc_expiry_year,$cc_issue_month,$cc_issue_year);

        $error = "";
        switch ($check_result) {
            case -1:
                $error = sprintf(TEXT_CCVAL_ERROR_UNKNOWN_CARD, substr($cc_validation->cc_number, 0, 4));
                break;
            case -2:
                $error = sprintf(TEXT_CCVAL_ERROR_INVALID_MONTH_EXPIRY, $cc_expiry_month);
                break;
            case -3:
                $error = sprintf(TEXT_CCVAL_ERROR_INVALID_YEAR_EXPIRY, $cc_expiry_year);
                break;
            case -4:
                $error = sprintf(TEXT_CCVAL_ERROR_INVALID_YEAR_EXPIRY, $cc_expiry_year);
                break;
            case false:
                $error = TEXT_CCVAL_ERROR_INVALID_NUMBER;
                break;
        }

        if (($check_result === false) || ($check_result < 1)) {
            $this->zcLog('before_process - DP-2', 'CC validation results: ' . $error . '(' . $check_result . ')');
            $messageStack->add_session($this->code, $error . '<!-- [' . $this->code . '] -->' . '<!-- result: ' . $check_result . ' -->', 'error');
            zen_redirect(zen_href_link(FILENAME_CHECKOUT_PAYMENT, '', 'SSL', true, false));
        }
        if (!in_array($cc_validation->cc_type, array('Visa', 'MasterCard', 'Switch', 'Solo', 'Discover', 'American Express', 'Maestro'))) {
            $messageStack->add_session($this->code, MODULE_PAYMENT_BRAINTREE_TEXT_BAD_CARD . '<!-- [' . $this->code . ' ' . $cc_validation->cc_type . '] -->', 'error');
            zen_redirect(zen_href_link(FILENAME_CHECKOUT_PAYMENT, '', 'SSL', true, false));
        }

        return $cc_validation;
    }
}

/**
 * this is ONLY here to offer compatibility with ZC versions prior to v1.5.2
 */
if (!function_exists('plugin_version_check_for_updates')) {

    function plugin_version_check_for_updates($fileid = 0, $version_string_to_check = '') {
        if ($fileid == 0)
            return FALSE;
        $new_version_available = FALSE;
        $lookup_index = 0;
        $url = 'http://www.zen-cart.com/downloads.php?do=versioncheck' . '&id=' . (int) $fileid;
        $data = json_decode(file_get_contents($url), true);
        // compare versions
        if (strcmp($data[$lookup_index]['latest_plugin_version'], $version_string_to_check) > 0)
            $new_version_available = TRUE;
        // check whether present ZC version is compatible with the latest available plugin version
        if (!in_array('v' . PROJECT_VERSION_MAJOR . '.' . PROJECT_VERSION_MINOR, $data[$lookup_index]['zcversions']))
            $new_version_available = FALSE;
        return ($new_version_available) ? $data[$lookup_index] : FALSE;
    }

}

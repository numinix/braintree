<?php

// Google Pay Module Language File for Zen Cart (Both Admin and Frontend)

$define = [
    // Admin side text
    'MODULE_PAYMENT_BRAINTREE_GOOGLE_PAY_TEXT_ADMIN_TITLE' => (IS_ADMIN_FLAG === true) ? '<strong>Google Pay</strong><br /><a href="https://www.braintreepayments.com/" target="_blank">Manage your Braintree account.</a><br />' : 'Google Pay',
    'MODULE_PAYMENT_BRAINTREE_GOOGLE_PAY_TEXT_ADMIN_DESCRIPTION' => 'Pay with Google Pay',

    // Error and success messages
    'MODULE_PAYMENT_BRAINTREE_GOOGLE_PAY_ERROR_HEADING' => 'We\'re sorry, but we were unable to process your payment.',
    'MODULE_PAYMENT_BRAINTREE_GOOGLE_PAY_TEXT_CARD_ERROR' => 'The card information entered contains an error. Please check and try again.',

    'MODULE_PAYMENT_BRAINTREE_GOOGLE_PAY_PAYMENT_FAILED' => 'Payment via Google Pay failed. Please try again.',
    'MODULE_PAYMENT_BRAINTREE_GOOGLE_PAY_PAYMENT_SUCCESS' => 'Payment successfully processed via Google Pay.',

    // Additional info for card details in Admin (these could be adjusted to match your needs)
    'MODULE_PAYMENT_BRAINTREE_GOOGLE_PAY_TEXT_CREDIT_CARD_FIRSTNAME' => 'Cardholder First Name:',
    'MODULE_PAYMENT_BRAINTREE_GOOGLE_PAY_TEXT_CREDIT_CARD_LASTNAME' => 'Cardholder Last Name:',
    'MODULE_PAYMENT_BRAINTREE_GOOGLE_PAY_TEXT_CREDIT_CARD_NUMBER' => 'Card Number:',
    'MODULE_PAYMENT_BRAINTREE_GOOGLE_PAY_TEXT_EXPIRATION_DATE' => 'Expiration Date:',
    'MODULE_PAYMENT_BRAINTREE_GOOGLE_PAY_TEXT_CVC' => 'CVC:',

    // Admin notification messages
    'NOTIFY_PAYMENT_BRAINTREE_UNINSTALLED' => 'Braintree payment module uninstalled successfully.',
];

?>